<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EGrupos extends Model
{
    use HasFactory;

    protected $table      = 'E_GRUPOS';
    protected $primaryKey = 'GRUP_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'GRUP_ID',
        'GRUP_NOME',
        'GRUP_ATUALIZACAO',
        'GRUP_USUARIO',
        'GRUP_ESTACAO',
        'GRUP_ESTACAO_CONTA',
        'GRUP_ASSINATURA',
        'GRUP_IMPORTACAO_ID',
        'GRUP_E_COM_PLUS_ID',
        'GRUP_ML_CATEGORIA_ID',
        'GRUP_GOOGLE_PROD_CATEGORIA_ID',
        'GRUP_ARVORE_CATEGORIA',
    ];
}

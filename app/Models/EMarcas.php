<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EMarcas extends Model
{
    use HasFactory;

    protected $table      = 'E_MARCAS';
    protected $primaryKey = 'MARC_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'MARC_ID',
        'MARC_NOME',
        'MARC_ATUALIZACAO',
        'MARC_USUARIO',
        'MARC_ESTACAO',
        'MARC_ESTACAO_CONTA',
        'MARC_ASSINATURA',
        'MARC_IMPORTACAO_ID',
        'MARC_LOGO_URL',
        'MARC_LOGO_TAMANHO',
        'MARC_E_COM_PLUS_ID',
    ];
}

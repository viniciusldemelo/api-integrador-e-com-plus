<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EPessoas extends Model
{
    use HasFactory;

    protected $table      = 'E_PESSOAS';
    protected $primaryKey = 'PESS_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'PESS_ID',
        'CPRC_ID',
        'EMPR_ID',
        'UNEM_ID',
        'PESS_NOME_RAZAO_SOCIAL',
        'PESS_APELIDO_FANTASIA',
        'PESS_CNPJ_CPF',
        'PESS_TIPO',
        'PESS_TIPO_CONSUMO',
        'PESS_TIPO_COMPRA',
        'PESS_DESCONTO_INCONDICIONADO',
        'PESS_EMAIL',
        'PESS_OBSERVACAO',
        'PESS_SITUACAO',
        'PESS_DATA_CADASTRO',
        'PESS_LINK_SITE',
        'PESS_LINK_FACEBOOK',
        'PESS_LINK_YOUTUBE',
        'PESS_LINK_INSTAGRAM',
        'PESS_LINK_TWITTER',
        'PESS_LINK_REDE_SOCIAL',
        'PESS_ATUALIZACAO',
        'PESS_USUARIO',
        'PESS_ESTACAO',
        'PESS_ESTACAO_CONTA',
        'PESS_ASSINATURA',
        'PESS_IMPORTACAO_ID',
        'PESS_E_COM_PLUS_ID',
    ];
}

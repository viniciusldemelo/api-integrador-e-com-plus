<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ESubGrupos extends Model
{
    use HasFactory;

    protected $table      = 'E_SUB_GRUPOS';
    protected $primaryKey = 'SBGR_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'SBGR_ID',
        'SBGR_NOME',
        'SBGR_ATUALIZACAO',
        'SBGR_USUARIO',
        'SBGR_ESTACAO',
        'SBGR_ESTACAO_CONTA',
        'SBGR_ASSINATURA',
        'SBGR_IMPORTACAO_ID',
        'SBGR_E_COM_PLUS_ID',
    ];
}

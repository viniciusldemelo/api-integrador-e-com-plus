<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EProdutos extends Model
{
    use HasFactory;

    protected $table      = 'E_PRODUTOS';
    protected $primaryKey = 'PROD_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'PROD_ID',
        'PROD_CODIGO',
        'PROD_NOME',
        'PROD_NOME_COMERCIAL',
        'PROD_NOME_RESUMIDO',
        'PROD_DESCRICAO',
        'PROD_TIPO_ITEM',
        'PROD_ORIGEM',
        'PROD_NVE',
        'PROD_EX_TIPI',
        'PROD_INFORMACAO_ADICIONAL',
        'PROD_INFO_COMPLEMENTAR',
        'PROD_PALAVRAS_CHAVES',
        'PROD_CODIGO_BALANCA',
        'PROD_CODIGO_LISTA_SERVICO',
        'PROD_CODIGO_ANP',
        'PROD_DESCRICAO_ANP',
        'PROD_COMB_GLP',
        'PROD_COMB_GNN',
        'PROD_COMB_GNI',
        'PROD_COMB_PARTIDA_VALOR',
        'PROD_COD_TRIB_MUNICIPAL',
        'PROD_INCENTIVO_FISCAL',
        'PROD_MOVIMENTA_ESTOQUE',
        'PROD_MOVIMENTO_NEGATIVO',
        'PROD_APRESENTACAO',
        'PROD_OBSERVACAO',
        'PROD_ATUALIZACAO',
        'PROD_USUARIO',
        'PROD_ESTACAO',
        'PROD_ESTACAO_CONTA',
        'PROD_E_COM_PLUS_ID',
        'PROD_TIPO_MERCADORIA',
        'PROD_I18N',
        'PROD_LINK_PERMANENTE',
        'PROD_LINK_MOBILE',
        'PROD_STATUS',
        'PROD_ACESSIVEL',
        'PROD_VISIVEL',
        'PROD_RELEVANCIA_ANUNCIO',
        'PROD_TIPO_ANUNCIO ',
        'PROD_CORPO_HTML ',
        'PROD_CORPO_TEXTO',
        'PROD_TITULO_MARCADOR',
        'PROD_DESCRICAO_MARCADOR',
        'PROD_GERENCIAR_ESTOQUE',
        'PROD_CONDICAO',
        'PROD_USO_ADULTO',
        'PROD_GARANTIA',
        'PROD_ML_CATEGORIA_ID',
        'PROD_GOOGLE_PROD_CATEGORIA_ID',
        'PROD_PROD_ID_ITEM_PAI',
        'PROD_PRODUTOS_RELAC_PREENC_AUTO',
        'PROD_FRETE_GRATIS',
        'PROD_PALAVRAS_CHAVE',
    ];
}

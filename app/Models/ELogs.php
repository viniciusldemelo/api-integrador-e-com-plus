<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ELogs extends Model
{
    use HasFactory;

    protected $table      = 'E_LOGS';
    protected $primaryKey = 'LOGS_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'LOGS_ID',
        'LOGS_NOME',
        'LOGS_DESCRICAO',
    ];
}

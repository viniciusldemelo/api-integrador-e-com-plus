<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EArquivosECommerce extends Model
{
    use HasFactory;

    protected $table      = 'E_ARQUIVOS_E_COMMERCE';
    protected $primaryKey = 'AREC_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'AREC_ID',
        'PROD_ID',
        'PRMC_ID',
        'PRGD_ID',
        'PRUN_ID',
        'MARC_ID',
        'AREC_E_COM_PLUS_ID',
        'AREC_URL_VIDEO',
        'AREC_URL_VIDEO_TITULO',
        'AREC_URL_ANEXO',
        'AREC_URL_ANEXO_TITULO',
        'AREC_ATUALIZACAO',
        'AREC_USUARIO',
        'AREC_ESTACAO',
        'AREC_ESTACAO_CONTA',
        'AREC_ASSINATURA',
    ];
}

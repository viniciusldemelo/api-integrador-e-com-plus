<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EDocumentosAuxiliaresItens extends Model
{
    use HasFactory;

    protected $table      = 'E_DOCUMENTOS_AUXILIARES_ITENS';
    protected $primaryKey = 'DCAI_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'DCAI_ID',
        'DCAV_ID',
        'CPRC_ID',
        'EMPR_ID',
        'UNEM_ID',
        'DCAI_ITEM',
        'DCAI_CODIGO_PRODUTO_SERVICO',
        'DCAI_NOME_PRODUTO_SERVICO',
        'DCAI_UNIDADE',
        'DCAI_QUANTIDADE',
        'DCAI_MULTIPLICADOR',
        'DCAI_VALOR_CUSTO',
        'DCAI_VALOR_UNITARIO_BASE',
        'DCAI_VALOR_UNITARIO_VENDA',
        'DCAI_VALOR_TOTAL_BRUTO',
        'DCAI_VALOR_DESCONTO_VENDA',
        'DCAI_VALOR_DESCONTO_ITEN',
        'DCAI_VALOR_DESCONTO_CONDICIONAL',
        'DCAI_VALOR_FRETE_VENDA',
        'DCAI_VALOR_FRETE_ITEN',
        'DCAI_VALOR_SEGURO_VENDA',
        'DCAI_VALOR_SEGURO_ITEN',
        'DCAI_VALOR_DESPESAS_VENDA',
        'DCAI_VALOR_DESPESAS_ITEN',
        'DCAI_ICMS_VALOR',
        'DCAI_ICMS_ST_VALOR',
        'DCAI_FCP_VALOR',
        'DCAI_FCP_ST_VALOR',
        'DCAI_II_VALOR',
        'DCAI_IPI_VALOR',
        'DCAI_ISSQN_RETIDO',
        'DCAI_PIS_VALOR',
        'DCAI_COFINS_VALOR',
        'DCAI_VALOR_TOTAL_LIQUIDO',
        'DCAI_MOVIMENTA_ESTOQUE',
        'DCAI_TIPO_COMPRA',
        'DCAI_KIT',
        'DCAI_PRODUCAO',
        'DCAI_PRAZO_MEDIO_ENTREGA',
        'DCAI_DATA_EMISSAO',
        'DCAI_CANCELADO',
        'DCAI_CANCELADO_DATA',
        'DCAI_TIPO_ITEM',
        'DCAI_NCM',
        'DCAI_CODIGO_LISTA_SERVICO',
        'DCAI_PESO_LIQUIDO',
        'DCAI_PESO_BRUTO',
        'DCAI_OBSERVACAO',
        'DCAI_ATUALIZACAO',
        'DCAI_USUARIO',
        'DCAI_ESTACAO',
        'DCAI_ESTACAO_CONTA',
        'DCAI_ASSINATURA',
        'DCAI_IMPORTACAO_ID',        
        'DCAV_E_COM_PLUS_ID',
        'DCAI_E_COM_PLUS_ID',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EConfiguracoesSistemas extends Model
{
    use HasFactory;

    protected $table      = 'E_CONFIGURACOES_SISTEMAS';
    protected $primaryKey = 'CFST_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'CFST_E_COM_PLUS_ID',
        'CFST_E_COM_PLUS_STORE_ID',
        'CFST_E_COM_PLUS_API_KEY',
        'CFST_E_COM_PLUS_ACCESS_TOKEN',
        'CFST_E_COM_PLUS_EXPIRACAO_TOKEN',
    ];
}

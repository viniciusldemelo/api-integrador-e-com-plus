<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EDocumentosAuxiliaresVendas extends Model
{
    use HasFactory;

    protected $table      = 'E_DOCUMENTOS_AUXILIARES_VENDAS';
    protected $primaryKey = 'DCAV_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'DCAV_ID',
        'CPRC_ID',
        'EMPR_ID',
        'UNEM_ID',
        'USRS_ID',
        'PESS_ID',
        'DCAV_NUMERO',
        'DCAV_DATA_EMISSAO',
        'DCAV_SITUACAO',
        'DCAV_CPF_CNPJ',
        'DCAV_NOME',
        'DCAV_TELEFONE',
        'DCAV_EMAIL',
        'DCAV_PESSOA_TIPO',
        'DCAV_TIPO_CONSUMO',
        'DCAV_TIPO_COMPRA',
        'DCAV_MODALIDADE_FRETE',
        'DCAV_INDICADOR_PRESENCA',
        'DCAV_REFERENCIA',
        'DCAV_VALOR_FRETE',
        'DCAV_VALOR_SEGURO',
        'DCAV_VALOR_DESCONTO',
        'DCAV_VALOR_DESCONTO_CONDICIONAL',
        'DCAV_VALOR_DESPESAS',
        'DCAV_CONTROLE',
        'DCAV_INDICADOR_IE',
        'DCAV_ISSQN_RETEM',
        'DCAV_ENTR_CEP',
        'DCAV_ENTR_NUMERO',
        'DCAV_ENTR_LOGRADOURO',
        'DCAV_ENTR_COMPLEMENTO',
        'DCAV_ENTR_BAIRRO',
        'DCAV_ENTR_REFERENCIA',
        'DCAV_ENTR_TELEFONE',
        'DCAV_ENTR_CONTATO',
        'DCAV_TRAN_QUANTIDADE_VOLUME',
        'DCAV_OBSERVACAO',
        'DCAV_OBSERVACAO_NOTA',
        'DCAV_SITUACAO_DATA',
        'DCAV_SITUACAO_MENSAGEM',
        'DCAV_CHAVE_DESCONTO',
        'DCAV_USRS_DESCONTO',
        'DCAV_ATUALIZACAO',
        'DCAV_USUARIO',
        'DCAV_ESTACAO',
        'DCAV_ESTACAO_CONTA',
        'DCAV_IMPORTACAO_ID',
        'DCAV_E_COM_PLUS_ID',
        'DCAV_E_COM_PLUS_ID',
        'DCAV_STATUS_FINANCEIRO_E_COMMER'
    ];
}

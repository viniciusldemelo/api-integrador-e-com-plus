<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EDocumentosAuxiliaresPagar extends Model
{
    use HasFactory;

    protected $table      = 'E_DOCUMENTOS_AUXILIARES_PAGAR';
    protected $primaryKey = 'DCAP_ID';
    protected $connection = 'firebird';
    public $timestamps    = false;
    public $incrementing  = false;

    protected $fillable = [
        'DCAP_ID',
        'DCAV_ID',
        'CPRC_ID',
        'EMPR_ID',
        'UNEM_ID',
        'USRS_ID',
        'FRPG_ID',
        'PESS_ID',
        'PESS_PAGD_ID',
        'DCAP_FORMA_PAGAMENTO',
        'DCAP_VALOR',
        'DCAP_QUANTIDADE',
        'DCAP_DATA_VENCIMENTO',
        'DCAP_INTERVALO_DIAS',
        'DCAP_JUROS_TAXA',
        'DCAP_JUROS_DIAS',
        'DCAP_JUROS',
        'DCAP_TARIFAS',
        'DCAP_VALOR_TOTAL',
        'DCAP_DATA_REFERENCIA',
        'DCAP_ENTRADA',
        'DCAP_CREDITO',
        'DCAP_AGENCIA',
        'DCAP_CONTA',
        'DCAP_DOCUMENTO_NUMERO',
        'DCAP_DOCUMENTO_DATA',
        'DCAP_DOCUMENTO_EMISSAO',
        'DCAP_TITULAR_CONTA_DESDE',
        'DCAP_CONFIRMADO_LIBERADO',
        'DCAP_COMPROVANTE',
        'DCAP_ATUALIZACAO',
        'DCAP_USUARIO',
        'DCAP_ESTACAO',
        'DCAP_ESTACAO_CONTA',
        'DCAP_ASSINATURA',
        'DCAP_IMPORTACAO_ID',
        'DCAP_STATUS_FINANCEIRO_E_COMMER',
        'DCAV_E_COM_PLUS_ID',
    ];
}

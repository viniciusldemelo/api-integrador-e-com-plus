<?php

namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use App\Models\ELogs;
use Illuminate\Http\Request;

class ELogsController extends Controller
{
    private $eComPlusService;

    public function __construct()
    {
        $this->eComPlusService = new EComPlusService();
    }

    /**
     * Obter logs
     *
     * @return \Illuminate\Http\Response
     */
    public function obterLogs()
    {
        $eLogs = ELogs::select('LOGS_NOME', 'LOGS_DESCRICAO', 'LOGS_DATA_HORA')->orderBy('LOGS_ID', 'desc')->get();

        $dados = [];

        foreach ($eLogs as $eLog) {
            array_push($dados, ['LOG_DESCRICAO' => converterTextoParaUTF8($eLog['LOG_DESCRICAO'])]);
        }

        return response()->json($dados);
    }

    /**
     * Salvar log
     *
     * @param Request $request
     */
    public function salvarLog(Request $request)
    {
        $dados = $request->all();

        $this->eComPlusService->gerarLog($dados['mensagem']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use App\Models\EDocumentosAuxiliaresVendas;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EDocumentosAuxiliaresVendasController extends Controller
{
    /**
     * Importar todos os pedidos
     *
     * @return \Illuminate\Http\Response
     */
    public static function importarPedidos()
    {
        $eComPlusService = new EComPlusService();

        $pedidosEComPlus = $eComPlusService->pesquisarTodosPedidos();
        // return response()->json($pedidosEComPlus);
        $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

        $pedidos                             = [];
        $temPedidoInconsistente              = false;
        $itensPedidoSemProdutoRelacionadoERP = false;

        foreach ($pedidosEComPlus['result'] as $pedido) {
            foreach ($pedido['items'] as $item) {
                $dadosProduto = DB::table('E_PRODUTOS_UNIDADES AS PRUN')
                    ->join('E_PRODUTOS AS PROD', 'PRUN.PROD_ID', '=', 'PROD.PROD_ID')
                    ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
                    ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
                    ->select('PROD.PROD_NOME_COMERCIAL')
                    ->where('PRUN.PRUN_CODIGO', $item['sku'])
                    ->where('PRUN.UNEM_ID', $configSistema['UNEM_ID'])
                    ->get();

                if (count($dadosProduto) == 0) {
                    $eComPlusService->gerarLog('Pedido ' . $pedido['_id'] . ' => Item: ' . $item['sku'] . ' - ' . $item['name'] . ' não está relacionado a nenhum produto do ERP.');
                    $itensPedidoSemProdutoRelacionadoERP = count($dadosProduto) == 0 ? true : false;
                    $temPedidoInconsistente              = true;
                }
            }

            if (!$itensPedidoSemProdutoRelacionadoERP) {
                $_id                   = isset($pedido['_id']) ? $pedido['_id'] : '';
                $number                = isset($pedido['number']) ? $pedido['number'] : '';
                $buyers                = isset($pedido['buyers']) ? $pedido['buyers'] : '';
                $items                 = isset($pedido['items']) ? $pedido['items'] : '';
                $amount                = isset($pedido['amount']) ? $pedido['amount'] : '';
                $shipping_method_label = isset($pedido['shipping_method_label']) ? $pedido['shipping_method_label'] : '';
                $payment_method_label  = isset($pedido['payment_method_label']) ? $pedido['payment_method_label'] : '';
                $financial_status      = isset($pedido['financial_status']) ? $pedido['financial_status']['current'] : '';
                $notes                 = isset($pedido['notes']) ? $pedido['notes'] : '';
                $created_at            = isset($pedido['created_at']) ? $pedido['created_at'] : '';
                $updated_at            = isset($pedido['updated_at']) ? $pedido['updated_at'] : '';

                array_push($pedidos, [
                    '_id'               => $_id,
                    'numero_pedido'     => $number,
                    'clientes'          => $buyers,
                    'itens_pedido'      => $items,
                    'montante'          => $amount,
                    'tipo_entrega'      => $shipping_method_label,
                    'forma_pagamento'   => $payment_method_label,
                    'status_financeiro' => $financial_status,
                    'notas_pedido'      => $notes,
                    'created_at'        => $created_at,
                    'updated_at'        => $updated_at,
                ]);
            }

            $itensPedidoSemProdutoRelacionadoERP = false;
        }

        if ($temPedidoInconsistente && count($pedidos) > 0) {
            // Salvar pedido
            $resposta = $eComPlusService->salvarPedido($pedidos);
            // return response()->json($resposta);
            $resposta = $resposta . ' Há pedido(s) inconsistente(s).';

            // $eComPlusService->gerarLog($resposta);
            // return response()->json($resposta);
        } else if (!$temPedidoInconsistente && count($pedidos) > 0) {
            // Salvar pedido
            $resposta = $eComPlusService->salvarPedido($pedidos);

            // $eComPlusService->gerarLog($resposta);
            // return response()->json($resposta);
        } else {
            $resposta = 'Não há pedidos para importar.';
            // $eComPlusService->gerarLog($resposta);
            // return response()->json($resposta);
        }
    }

    public function listarPedidosImportados()
    {
        return response()->json(EDocumentosAuxiliaresVendas::select('DCAV_NOME', 'DCAV_NUMERO', 'DCAV_E_COM_PLUS_ID')->whereNotNull('DCAV_E_COM_PLUS_ID')->get());
    }
}

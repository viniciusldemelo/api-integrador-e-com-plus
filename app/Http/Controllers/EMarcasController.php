<?php
namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use App\Models\EArquivosECommerce;
use App\Models\EMarcas;
use Illuminate\Contracts\Routing\ResponseFactory;

class EMarcasController extends Controller
{

    /**
     * Envia todos as marcas atualizadas até cinco minutos atrás
     *
     * @return \Illuminate\Http\Response
     */
    public static function enviarMarcas()
    {
        try {
            ini_set('max_execution_time', '600'); // 600 segundos == 10 minutos

            // DB::enableQueryLog();
            /* Marcas */
            $eComPlusService = new EComPlusService();
            $marcas = $eComPlusService->obterMarcas();
            // return DB::getQueryLog();

            if (count($marcas) == 0) {
                // $eComPlusService->gerarLog('Sem marcas atualizadas para envio.');
                // return response()->json('Sem marcas atualizadas para envio.');
                return 'sem marcas';
            }

            $name = '';
            $slug = '';

            foreach ($marcas as $marca) {
                $marcaEnviada = false;
                $name = utf8_encode($marca->NAME);
                $slug = formatarSlug($name);

                // return response()->json($marca->ID);

                $dadosMarca = [
                    'name' => $name,
                    'slug' => $slug,
                    'logo' => [
                        'url' => $marca->LOGO_URL != '' ? $marca->LOGO_URL : 'https://i.ibb.co/CpyZbXt/produto-sem-imagem.webp',
                        'size' => $marca->LOGO_SIZE != '' ? $marca->LOGO_SIZE : '592x432'
                    ]
                ];

                if (is_null($marca->E_COM_PLUS_ID)) {
                    try {
                        // return response()->json($dadosMarca);
                        $resposta = json_decode($eComPlusService->criarMarca($dadosMarca), true);
                        $marcaEnviada = true;
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Marca  ' . $name . ' não foi enviada. Detalhes: ' . $th->getMessage());
                        continue;
                    }

                    try {
                        $eComPlusService->atualizarIdEComPlusMarcaEnviada($marca->ID, $resposta['_id']);
                        $resposta = 'Marca ' . $marca->ID . ' - ' . $name . ', slug: ' . $slug . ' enviada. Novo _id: ' . $resposta['_id'];
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('MARC_E_COM_PLUS_ID da marca ' . $name . ', slug: ' . $slug . ' não foi salvo. Detalhes: ' . $th->getMessage());
                    }
                } else {
                    try {
                        // return response()->json($dadosMarca);
                        $eComPlusService->editarMarca($marca->E_COM_PLUS_ID, $dadosMarca);
                        $marcaEnviada = true;
                        $resposta = 'Marca ' . $marca->ID . ' - ' . $name . ', slug: ' . $slug . ' atualizada.';
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Marca ' . $name . ', slug: ' . $slug . ' não foi atualizada. Detalhes: ' . $th->getMessage());
                    }
                }

                $eComPlusService->gerarLog($resposta);

                // return response()->json($resposta);
            }

            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto

            if ($marcaEnviada) {
                return response()->json('Marcas enviadas e/ou atualizadas.');
                return response()->json('Marcas enviadas e/ou atualizadas.');
            } else {
                return response()->json('Nenhuma marca enviada e/ou atualizada.');
                return response()->json('Nenhuma marca enviada e/ou atualizada.');
            }
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao enviar/atualizar marca(s). Detalhes: ' . $th->getMessage());
            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
            return response()->json('Erro ao enviar/atualizar marca(s). Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Enviar imagens de marcas
     *
     * @return ResponseFactory
     */
    public static function enviarImagensMarcas()
    {
        ini_set('max_execution_time', '1200'); // 1200 segundos == 20 minutos

        $eComPlusService = new EComPlusService();

        $pathImagensMarcas = env('PATH_IMAGENS_MARCAS', '') . 'imagens_marcas_para_envio.txt';

        if (file_exists($pathImagensMarcas)) {
            if ($arquivo = fopen($pathImagensMarcas, 'r')) {
                $imagemEnviada = false;

                while ($linha = fgets($arquivo)) {
                    if ($linha != '') {
                        try {
                            $nomeArquivoExtensao = $linha[strlen($linha) - 1] == "\n" ? substr($linha, 0, strlen($linha) - 2) : $linha;
                            $pathArquivoImagem = pathinfo(env('PATH_IMAGENS_MARCAS', '') . $nomeArquivoExtensao);
                            $nomeImagem = $pathArquivoImagem['filename'];
                            $extensao = $pathArquivoImagem['extension'];

                            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

                            $eMarca = EMarcas::find(intval($nomeImagem));

                            if (! isset($eMarca->MARC_ID)) {
                                $eComPlusService->gerarLog('Marca não encontrada. Imagem: ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            $eArquivosECommerce = EArquivosECommerce::select('AREC_ID')->where('MARC_ID', intval($nomeImagem))->get();

                            if (count($eArquivosECommerce) > 0 && ! env('ATUALIZAR_IMAGENS_MARCAS', false)) {
                                $eComPlusService->gerarLog('Imagem já existe no storage de imagens. Imagem: ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            if (! file_exists(env('PATH_IMAGENS_MARCAS', '') . $nomeArquivoExtensao)) {
                                $eComPlusService->gerarLog('Imagem não encontrada. Path: ' . env('PATH_IMAGENS_MARCAS', '') . $nomeArquivoExtensao);
                                continue;
                            }

                            if ($nomeImagem == '' || is_null($nomeImagem)) {
                                $eComPlusService->gerarLog('Nome de arquivo inválido. Imagem ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            if ($extensao == 'jpg') {
                                $extensao = 'jpeg';
                            }

                            $responseEnvio = $eComPlusService->enviarImagemApxStorage($configSistema, env('PATH_IMAGENS_MARCAS', '') . $nomeArquivoExtensao, $nomeImagem, $extensao);

                            if ($responseEnvio == '') {
                                $eComPlusService->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' não foi enviada.');
                                continue;
                            }
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Erro ao enviar imagem de marca ao apx-storage. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                            return response()->json('Erro ao enviar imagem de marca ao apx-storage. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                        }

                        try {
                            sleep(1);

                            // Dados de largura e altura da imagem
                            $dadosImagem = getimagesize(env('PATH_IMAGENS_MARCAS', '') . $nomeArquivoExtensao);
                            $largura = $dadosImagem[0];
                            $altura = $dadosImagem[1];

                            // Salvar URLs das imagens de marcas
                            $eComPlusService->salvarImagemMarca($responseEnvio, $configSistema['UNEM_ID'], $eMarca->MARC_ID, $nomeImagem, $extensao, $largura, $altura);

                            $eComPlusService->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' enviada/atualizada com sucesso.');

                            $imagemEnviada = true;
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Erro ao salvar imagem de marca. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                            return response()->json('Erro ao salvar imagem de marca. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                        }
                    }
                }

                // Limpa o conteúdo do arquivo de texto
                file_put_contents(env('PATH_IMAGENS_MARCAS', '') . 'imagens_marcas_para_envio.txt', '');

                if ($imagemEnviada) {
                    $eComPlusService->gerarLog('Imagem/imagens de marcas enviada(s) e/ou atualizada(s).');
                    // return response()->json('Imagens de marcas enviadas e/ou atualizadas.');
                } else {
                    // $eComPlusService->gerarLog('Nenhuma imagem de marca foi enviada e/ou atualizada.');
                    // return response()->json('Nenhuma imagem de marca foi enviada e/ou atualizada.');
                }
            }
        } else {
            $eComPlusService->gerarLog('Arquivo imagens_marcas_para_envio.txt não encontrado.');
            return response()->json('Arquivo imagens_marcas_para_envio.txt não encontrado.');
        }

        ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
    }
}

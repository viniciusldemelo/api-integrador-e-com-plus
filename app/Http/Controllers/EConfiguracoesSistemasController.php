<?php

namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use App\Models\EConfiguracoesSistemas;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EConfiguracoesSistemasController extends Controller
{
    private $eComPlusService;

    public function __construct()
    {
        $this->eComPlusService = new EComPlusService();
    }
    /**
     * Gerar token de acesso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function gerarTokenAcesso(Request $request)
    {
        try {
            $dados         = $request->all();
            $configSistema = $this->obterConfiguracaoSistema();

            if ($configSistema['CFST_E_COM_PLUS_EXPIRACAO_TOKEN'] < Carbon::now() || is_null($configSistema['CFST_E_COM_PLUS_EXPIRACAO_TOKEN'])) {
                $novoTokenAcesso = json_decode(strval($this->eComPlusService->gerarNovoTokenAcesso($dados['CFST_E_COM_PLUS_ID'], $dados['CFST_E_COM_PLUS_API_KEY'])), true);

                $expiracaoToken = new Carbon($novoTokenAcesso['expires']);
                $expiracaoToken = $expiracaoToken->toDateTimeString();

                $eConfiguracoesSistemas = EConfiguracoesSistemas::select('*')->where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))->get()->first();

                if (!isset($eConfiguracoesSistemas)) {
                    $eConfiguracoesSistemas = new EConfiguracoesSistemas();

                    $eConfiguracoesSistemas->CFST_E_COM_PLUS_ACCESS_TOKEN    = $novoTokenAcesso['access_token'];
                    $eConfiguracoesSistemas->CFST_E_COM_PLUS_EXPIRACAO_TOKEN = $expiracaoToken;

                    $eConfiguracoesSistemas->save();
                } else {
                    EConfiguracoesSistemas::where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
                        ->update(
                            [
                                'CFST_E_COM_PLUS_ACCESS_TOKEN'    => $novoTokenAcesso['access_token'],
                                'CFST_E_COM_PLUS_EXPIRACAO_TOKEN' => $expiracaoToken,
                            ]
                        );
                }

                $this->eComPlusService->gerarLog('Novo token de acesso gerado.');

                return response()->json('Token atualizado');
            } else {
                $dataValida = Carbon::createFromFormat('Y-m-d H:i:s', $configSistema['CFST_E_COM_PLUS_EXPIRACAO_TOKEN'])->toArray();
                return response()->json('Token não foi atualizado, pois o atual ainda é válido até ' . $dataValida['day'] . '/' . $dataValida['month'] . '/' . $dataValida['year'] . ' ' . $dataValida['hour'] . ':' . $dataValida['minute'] . ':' . $dataValida['second']);
            }

        } catch (\Throwable $th) {
            $this->eComPlusService->gerarLog('Erro ao atualizar token de acesso. Detalhes: ' . $th->getMessage());
            return response()->json('Erro ao atualizar token de acesso. Detalhes: ' . $th->getMessage(), 500);
        }
    }

    /**
     * Validação do token de acesso
     *
     * @param  array $dados
     * @return \Illuminate\Http\Response
     */
    public static function validarTokenAcessoEComPlus($dados)
    {
        try {
            $eComPlusService = new EComPlusService;
            $configSistema   = self::obterConfiguracaoSistema();

            if ($configSistema['CFST_E_COM_PLUS_EXPIRACAO_TOKEN'] < Carbon::now() || is_null($configSistema['CFST_E_COM_PLUS_EXPIRACAO_TOKEN'])) {
                $novoTokenAcesso = json_decode(strval($eComPlusService->gerarNovoTokenAcesso($dados[0]['CFST_E_COM_PLUS_ID'], $dados[0]['CFST_E_COM_PLUS_API_KEY'])), true);

                $expiracaoToken = new Carbon($novoTokenAcesso['expires']);
                $expiracaoToken = $expiracaoToken->toDateTimeString();

                $eConfiguracoesSistemas = EConfiguracoesSistemas::select('*')->where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))->get()->first();

                if (!isset($eConfiguracoesSistemas)) {
                    $eConfiguracoesSistemas = new EConfiguracoesSistemas();

                    $eConfiguracoesSistemas->CFST_E_COM_PLUS_ACCESS_TOKEN    = $novoTokenAcesso['access_token'];
                    $eConfiguracoesSistemas->CFST_E_COM_PLUS_EXPIRACAO_TOKEN = $expiracaoToken;

                    $eConfiguracoesSistemas->save();
                } else {
                    EConfiguracoesSistemas::where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
                        ->update(
                            [
                                'CFST_E_COM_PLUS_ACCESS_TOKEN'    => $novoTokenAcesso['access_token'],
                                'CFST_E_COM_PLUS_EXPIRACAO_TOKEN' => $expiracaoToken,
                            ]
                        );
                }

                return [
                    'X-Access-Token' => $novoTokenAcesso['access_token'],
                ];
            } else {
                return [
                    'X-Access-Token' => $configSistema['CFST_E_COM_PLUS_ACCESS_TOKEN'],
                ];
            }

        } catch (\Throwable $th) {
            return response()->json('Erro ao atualizar novo token de acesso. Detalhes: ' . $th->getMessage(), 500);
        }
    }

    /**
     * Obter dados de Configuração de Sistema Por cfstEcomPlusStoreId
     *
     * @return mixed
     */
    public static function obterConfiguracaoSistema()
    {
        $systemConfiguration = EConfiguracoesSistemas::select('*')->where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))->get()->first();

        if ($systemConfiguration) {
            return [
                'CFST_E_COM_PLUS_ID'              => $systemConfiguration->CFST_E_COM_PLUS_ID,
                'CFST_E_COM_PLUS_API_KEY'         => $systemConfiguration->CFST_E_COM_PLUS_API_KEY,
                'CFST_E_COM_PLUS_ACCESS_TOKEN'    => $systemConfiguration->CFST_E_COM_PLUS_ACCESS_TOKEN,
                'CFST_E_COM_PLUS_EXPIRACAO_TOKEN' => $systemConfiguration->CFST_E_COM_PLUS_EXPIRACAO_TOKEN,
                'CPRC_ID'                         => $systemConfiguration->CPRC_ID,
                'EMPR_ID'                         => $systemConfiguration->EMPR_ID,
                'UNEM_ID'                         => $systemConfiguration->UNEM_ID,
            ];
        } else {
            return response()->json(['Sem dados de configurações de sistemas.']);
        }
    }
}

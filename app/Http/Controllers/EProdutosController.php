<?php
namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use Carbon\Carbon;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\DB;

class EProdutosController extends Controller
{

    /**
     * Envia todos os produtos atualizados até cinco minutos atrás
     *
     * @return \Illuminate\Http\Response
     */
    public static function enviarProdutos()
    {
        try {
            ini_set('max_execution_time', '600'); // 600 segundos == 10 minutos

            $eComPlusService = new EComPlusService();

            // DB::enableQueryLog();
            /* Produtos */
            $produtos = $eComPlusService->obterProdutos();
            // return DB::getQueryLog();

            // return response()->json($produtos);

            if (isset($produtos['mensagem']) && $produtos['mensagem'] == 'Sem dados') {
                // $eComPlusService->gerarLog('Sem produtos atualizados para envio.');
                // return response()->json('Sem produtos atualizados para envio.');
                return 'sem produtos';
            }

            $resposta = [];
            $sku      = '';
            $name     = '';
            $slug     = '';

            $produtoEnviado = false;

            foreach ($produtos as $produto) {
                $resposta = '';
                $sku      = $produto['SKU'];
                $name     = converterTextoParaUTF8($produto['NAME']);
                $slug     = utf8_encode($produto['SLUG']);
                $slug     = formatarSlug($produto['SLUG']);

                /* Palavras-chave (keywords) */
                $palavrasChaveProduto = [];
                $palavrasChaveProduto = explode(";", mb_strtolower($produto['KEYWORDS']));

                /* Dimensões (dimensions) */
                $dimensoesProduto = [];
                $dimensoes        = $eComPlusService->obterDimensoesPorProduto($produto['SKU']);

                foreach ($dimensoes as $dimensao) {
                    if (floatval($dimensao->WIDTH) > 0 && floatval($dimensao->HEIGHT) > 0 && floatval($dimensao->LENGTH) > 0) {
                        array_push($dimensoesProduto, [
                            'width'  => [
                                'value' => floatval($dimensao->WIDTH),
                            ],
                            'height' => [
                                'value' => floatval($dimensao->HEIGHT),
                            ],
                            'length' => [
                                'value' => floatval($dimensao->LENGTH),
                            ],
                        ]);
                    }
                }

                /* Marcas (brands) */
                $marcasProduto = [];
                $marcas        = $eComPlusService->obterMarcasPorProduto($produto['SKU']);

                if (count($marcas) == 0) {
                    $eComPlusService->gerarLog('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' sem marca cadastrada.');
                    continue;
                }

                foreach ($marcas as $marca) {
                    $slug = utf8_encode($marca->NAME);
                    $slug = formatarSlug($slug);

                    array_push($marcasProduto, [
                        '_id'  => $marca->E_COM_PLUS_ID,
                        'name' => converterTextoParaUTF8($marca->NAME),
                        'slug' => $slug,
                        'logo' => [
                            'url'  => $marca->LOGO_URL,
                            'size' => $marca->LOGO_SIZE,
                        ],
                    ]);
                }

                /* Categorias (categories) */
                $categoriasProduto = [];
                $categorias        = $eComPlusService->obterCategoriasPorProduto($produto['SKU']);

                if (count($categorias) == 0) {
                    $eComPlusService->gerarLog('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' sem categoria cadastrada.');
                    continue;
                }

                foreach ($categorias as $categoria) {
                    $slug = utf8_encode($categoria->NAME);
                    $slug = formatarSlug($slug);

                    array_push($categoriasProduto, [
                        '_id'  => $categoria->E_COM_PLUS_ID,
                        'name' => converterTextoParaUTF8($categoria->NAME),
                        'slug' => $slug,
                    ]);
                }

                /* Imagens (pictures) */
                $imagensProduto = [];
                $imagens        = $eComPlusService->obterImagensPorProduto($produto['SKU']);

                if (count($categorias) == 0) {
                    $eComPlusService->gerarLog('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' sem categoria cadastrada.');
                    continue;
                }

                foreach ($imagens as $imagem) {
                    array_push($imagensProduto, [
                        '_id'    => $imagem->E_COM_PLUS_ID,
                        'normal' => [
                            'alt' => $imagem->TITULO_IMAGEM,
                            'url' => $imagem->URL_IMAGEM,
                        ],
                    ]);
                }

                // return response()->json(converterTextoCp1252ToUtf8($produto['NAME']));

                /* Slug do produto */
                $slug = utf8_encode($produto['SLUG']);
                $slug = formatarSlug($produto['SLUG']);

                $dadosProduto = [
                    'sku'                  => $sku,
                    'name'                 => $name,
                    'slug'                 => $slug,
                    'short_description'    => converterTextoParaUTF8($produto['SHORT_DESCRIPTION']),
                    'body_html'            => $produto['BODY_HTML'],
                    'body_text'            => $produto['BODY_TEXT'],
                    'meta_title'           => $produto['META_TITLE'],
                    'meta_description'     => $produto['META_DESCRIPTION'],
                    'price_effective_date' => ['end' => Carbon::createFromFormat('Y-m-d H:i:s', $produto['PRICE_EFFECTIVE_DATE'])->toISOString()],
                    'price'                => floatval($produto['PRICE']),
                    'base_price'           => floatval($produto['BASE_PRICE']),
                    'commodity_type'       => 'physical',
                    
                    'permalink'            => 'https://tropicalgrupo.netlify.app/' . $slug,

                    // 'quantity'             => floatval($produto['QUANTITY']),
                    'quantity'             => 100,

                    // 'available'            => $produto['QUANTITY'] > 0 ? true : false,
                    'available'            => true,

                    // 'visible'              => $produto['QUANTITY'] > 0 ? true : false,
                    'visible'              => true,

                    'condition'            => 'new',
                    'gtin'                 => [$produto['GTIN']],
                    'weight'               => ['value' => floatval($produto['WEIGHT'])],
                    'keywords'             => $palavrasChaveProduto,
                    'dimensions'           => $dimensoesProduto,
                    'brands'               => $marcasProduto,
                    'categories'           => $categoriasProduto,
                    'pictures'             => $imagensProduto,
                ];

                if ($dadosProduto['gtin'] == [
                    '0',
                ]) {
                    unset($dadosProduto['gtin']);
                }

                if ($dadosProduto['weight'] == [
                    'value' => 0,
                ]) {
                    unset($dadosProduto['weight']);
                }

                if ($dadosProduto['keywords'] == []) {
                    unset($dadosProduto['keywords']);
                }

                if ($dadosProduto['dimensions'] == []) {
                    unset($dadosProduto['dimensions']);
                }

                if ($dadosProduto['brands'] == []) {
                    unset($dadosProduto['brands']);
                }

                if ($dadosProduto['categories'] == []) {
                    unset($dadosProduto['categories']);
                }

                // $buscaProduto = json_decode(strval($eComPlusService->encontrarProduto($produto['SKU'])), true);

                if (is_null($produto['E_COM_PLUS_ID'])) {
                    // if ($dadosProduto['sku'] == '1097') {
                    // return response()->json($dadosProduto);
                    // }

                    // return response()->json($dadosProduto);

                    try {
                        $resposta       = json_decode($eComPlusService->criarProduto($dadosProduto), true);
                        $produtoEnviado = true;
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' não foi enviado. Detalhes: ' . $th->getMessage());
                        // return response()->json('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' não foi enviado. Detalhes: ' . $th->getMessage());
                        continue;
                    }

                    try {
                        // return response()->json($resposta);
                        if (!is_null($resposta) || count($resposta) > 0) {
                            $eComPlusService->atualizarIdEComPlusProdutoEnviado($dadosProduto['sku'], $resposta['_id']);
                            $resposta = 'Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' enviado. Novo _id: ' . $resposta['_id'];
                        }
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('PROD_E_COM_PLUS_ID do produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' não foi salvo. Detalhes: ' . $th->getMessage());
                        // return response()->json('PROD_E_COM_PLUS_ID do produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' não foi salvo. Detalhes: ' . $th->getMessage());
                    }
                } else {
                    // if ($dadosProduto['sku'] == '1097') {
                    // return response()->json($dadosProduto);
                    // }

                    // return response()->json($dadosProduto);

                    try {
                        $eComPlusService->editarProduto($produto['E_COM_PLUS_ID'], $dadosProduto);
                        $resposta       = 'Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' atualizado.';
                        $produtoEnviado = true;
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Produto ' . $sku . ' - ' . $name . ', slug: ' . $slug . ' não foi atualizado. Detalhes: ' . $th->getMessage());
                    }
                }

                if (!is_null($resposta) || count($resposta) > 0) {
                    $eComPlusService->gerarLog($resposta);
                }

                // return response()->json($sku);
            }

            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto

            if ($produtoEnviado) {
                $eComPlusService->gerarLog('Produtos enviados e/ou atualizados.');
                return response()->json('Produtos enviados e/ou atualizados.');
            } else {
                $eComPlusService->gerarLog('Nenhum produto enviado e/ou atualizado.');
                return response()->json('Nenhum produto enviado e/ou atualizado.');
            }
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao enviar/atualizar produto(s). Detalhes: ' . $th->getMessage());
            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
            return response()->json('Erro ao enviar/atualizar produto(s). Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Enviar imagens de produtos
     *
     * @return ResponseFactory
     */
    public static function enviarImagensProdutos()
    {
        ini_set('max_execution_time', '1200'); // 1200 segundos == 20 minutos

        $eComPlusService = new EComPlusService();

        $pathImagens = env('PATH_IMAGENS_PRODUTOS', '') . 'imagens_produtos_para_envio.txt';

        if (file_exists($pathImagens)) {
            if ($arquivo = fopen($pathImagens, 'r')) {
                $imagemEnviada = false;

                while ($linha = fgets($arquivo)) {
                    if ($linha != '') {
                        try {
                            $nomeArquivoExtensao = $linha[strlen($linha) - 1] == "\n" ? substr($linha, 0, strlen($linha) - 2) : $linha;
                            $pathArquivoImagem   = pathinfo(env('PATH_IMAGENS_PRODUTOS', '') . $nomeArquivoExtensao);
                            $nomeImagem          = $pathArquivoImagem['filename'];
                            $extensao            = $pathArquivoImagem['extension'];

                            $produto = DB::select('
                                SELECT
                                  V_PROD.UNEM_ID,
                                  V_PROD.PRUN_ID,
                                  V_PROD.PROD_ID,
                                  V_PROD.PRMC_ID,
                                  V_PROD.PRGD_ID
                                FROM V_PRODUTOS V_PROD
                                  INNER JOIN E_CONFIGURACOES_SISTEMAS CFST ON
                                    V_PROD.UNEM_ID = CFST.UNEM_ID
                                WHERE
                                  CFST.CFST_E_COM_PLUS_STORE_ID = ?
                                  AND V_PROD.PRUN_ID = ?', [
                                env('E_COM_PLUS_STORE_ID', 0),
                                intval($nomeImagem),
                            ]);

                            if (count($produto) == 0) {
                                $eComPlusService->gerarLog('Produto não encontrado. Imagem: ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            $eArquivosECommerce = DB::select('
                                SELECT
                                  V_PROD.UNEM_ID,
                                  V_PROD.PRUN_ID,
                                  V_PROD.PROD_ID,
                                  V_PROD.PRMC_ID,
                                  V_PROD.PRGD_ID
                                FROM V_PRODUTOS V_PROD
                                  INNER JOIN E_CONFIGURACOES_SISTEMAS CFST ON
                                    V_PROD.UNEM_ID = CFST.UNEM_ID
                                  INNER JOIN E_ARQUIVOS_E_COMMERCE AREC ON
                                    V_PROD.UNEM_ID = AREC.UNEM_ID
                                    AND V_PROD.PRUN_ID = AREC.PRUN_ID
                                    AND V_PROD.PROD_ID = AREC.PROD_ID
                                    AND V_PROD.PRMC_ID = AREC.PRMC_ID
                                    AND V_PROD.PRGD_ID = AREC.PRGD_ID
                                WHERE
                                  CFST.CFST_E_COM_PLUS_STORE_ID = ?
                                  AND V_PROD.PRUN_ID = ?', [
                                env('E_COM_PLUS_STORE_ID', 0),
                                intval($nomeImagem),
                            ]);

                            if (count($eArquivosECommerce) > 0 && !env('ATUALIZAR_IMAGENS_PRODUTOS', false)) {
                                $eComPlusService->gerarLog('Imagem já existe no storage de imagens. Imagem: ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            if (!file_exists(env('PATH_IMAGENS_PRODUTOS', '') . $nomeArquivoExtensao)) {
                                $eComPlusService->gerarLog('Imagem não encontrada. Path: ' . env('PATH_IMAGENS_PRODUTOS', '') . $nomeArquivoExtensao);
                                continue;
                            }

                            if ($nomeImagem == '' || is_null($nomeImagem)) {
                                $eComPlusService->gerarLog('Nome de arquivo inválido. Imagem ' . $nomeImagem . '.' . $extensao);
                                continue;
                            }

                            if ($extensao == 'jpg') {
                                $extensao = 'jpeg';
                            }

                            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

                            $responseEnvio = $eComPlusService->enviarImagemApxStorage($configSistema, env('PATH_IMAGENS_PRODUTOS', '') . $nomeArquivoExtensao, $nomeImagem, $extensao);

                            if ($responseEnvio == '') {
                                $eComPlusService->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' não foi enviada.');
                                continue;
                            }
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Erro ao enviar imagem de produto ao apx-storage. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                            return response()->json('Erro ao enviar imagem de produto ao apx-storage. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                        }

                        try {
                            sleep(1);

                            // Salvar URLs das imagens de produtos
                            $eComPlusService->salvarImagemProduto($responseEnvio, $produto[0]->UNEM_ID, $produto[0]->PROD_ID, $produto[0]->PRMC_ID, $produto[0]->PRGD_ID, $produto[0]->PRUN_ID, $nomeImagem, $extensao);

                            $eComPlusService->gerarLog($nomeImagem . '.' . $extensao . ' enviado/atualizado com sucesso.');

                            $imagemEnviada = true;
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Erro ao salvar imagem de produto. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                            return response()->json('Erro ao salvar imagem de produto. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
                        }
                    }
                }

                // Limpa o conteúdo do arquivo de texto
                file_put_contents($pathImagens, '');

                if ($imagemEnviada) {
                    $eComPlusService->gerarLog('Imagem/imagens de produtos enviada(s) e/ou atualizada(s).');
                    return response()->json('Imagens de produtos enviadas e/ou atualizadas.');
                } else {
                    // $eComPlusService->gerarLog('Nenhuma imagem de produto foi enviada e/ou atualizada.');
                    // return response()->json('Nenhuma imagem de produto foi enviada e/ou atualizada.');
                }
            }
        } else {
            $eComPlusService->gerarLog('Arquivo imagens_produtos_para_envio.txt não encontrado.');
            return response()->json('Arquivo imagens_produtos_para_envio.txt não encontrado.');
        }

        ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
    }
}

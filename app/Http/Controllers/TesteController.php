<?php

namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use Illuminate\Http\Client\ResponseSequence;
use Illuminate\Support\Facades\DB;
use PDO;

class TesteController extends Controller
{
    public static function teste1()
    {
        $conexao = new PDO('firebird:dbname=' . env('DB_HOST', '') . ':' . env('DB_DATABASE', ''), env('DB_USERNAME', ''), env('DB_PASSWORD', ''));

        $query = $conexao->query('SELECT PRUN_ATUALIZACAO FROM E_PRODUTOS_UNIDADES WHERE PRUN_ID = 1086');
        $query->execute();
        $dados = $query->fetchAll(PDO::FETCH_OBJ);
        
        return response()->json($dados);
    }

    public static function teste2()
    {
        try {
            $produto = DB::select('
                SELECT 
                  PRUN.PROD_ID 
                FROM E_PRODUTOS_UNIDADES AS PRUN 
                WHERE PRUN.PRUN_CODIGO = ?', [
                213,
            ]);

            $produto = $produto[0]->PROD_ID;

            return response()->json($produto);

        } catch (\Throwable $th) {
            return response()->json('Erro. Detalhes: ' . $th->getMessage());
        }
    }
}

<?php
namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;
use App\Models\EGrupos;
use Illuminate\Support\Facades\DB;

class EGruposController extends Controller
{

    /**
     * Envia todos as categorias atualizadas até cinco minutos atrás
     *
     * @return \Illuminate\Http\Response
     */
    public static function enviarCategorias()
    {
        try {
            ini_set('max_execution_time', '600'); // 600 segundos == 10 minutos

            /* Categorias */
            // DB::enableQueryLog();
            $eComPlusService = new EComPlusService();
            $categoriasPai   = $eComPlusService->obterCategoriasPai();
            // return DB::getQueryLog();

            if (count($categoriasPai) == 0) {
                // $eComPlusService->gerarLog('Sem categorias atualizadas para envio.');
                // return response()->json('Sem categorias para atualização.');
                return 'sem categorias';
            }

            $countCategoriasPai = 0;
            $resposta = [];
            $name     = '';
            $slug     = '';

            foreach ($categoriasPai as $categoriaPai) {
                $categoriaEnviada = false;

                $name = converterTextoCp1252ToUtf8($categoriaPai->NAME);
                $slug = utf8_encode($categoriaPai->NAME);
                $slug = formatarSlug($slug);

                // return response()->json($categoria->ID);

                $dadosCategoriaPai = [
                    'name' => $name,
                    'slug' => $slug,
                ];

                if (is_null($categoriaPai->E_COM_PLUS_ID)) {
                    try {
                        // return response()->json($dadosCategoriaPai);

                        $countCategoriasPai = EGrupos::select(DB::raw('COUNT(GRUP_E_COM_PLUS_ID)'))->whereNotNull('GRUP_E_COM_PLUS_ID')->get();

                        $countCategoriasPai = $countCategoriasPai[0]->COUNT;

                        if ($countCategoriasPai >= 50) {
                            $eComPlusService->gerarLog('Não é possível enviar mais de 50 categorias.');
                            break;
                        }

                        $resposta         = json_decode($eComPlusService->criarCategoria($dadosCategoriaPai), true);
                        $categoriaEnviada = true;
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Categoria ' . $name . ' não foi enviada. Detalhes: ' . $th->getMessage());
                        continue;
                    }

                    try {
                        if (!is_null($resposta) || count($resposta) > 0) {
                            $eComPlusService->atualizarIdEComPlusCategoriaEnviada($categoriaPai->ID, $resposta['_id']);
                            $resposta = 'Categoria ' . $categoriaPai->ID . ' - ' . $name . ', slug: ' . $slug . ' enviada. Novo _id: ' . $resposta['_id'];
                        }
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('GRUP_E_COM_PLUS_ID da categoria ' . $name . ', slug: ' . $slug . ' não foi salvo. Detalhes: ' . $th->getMessage());
                    }
                } else {
                    try {
                        // return response()->json($dadosCategoriaPai);
                        $eComPlusService->editarCategoria($categoriaPai->E_COM_PLUS_ID, $dadosCategoriaPai);
                        $categoriaEnviada = true;
                        $resposta         = 'Categoria ' . $categoriaPai->ID . ' - ' . $name . ', slug: ' . $slug . ' atualizada.';
                    } catch (\Throwable $th) {
                        $eComPlusService->gerarLog('Categoria ' . $name . ', slug: ' . $slug . ' não foi atualizada. Detalhes: ' . $th->getMessage());
                    }
                }

                // $eComPlusService->gerarLog($resposta);
                // return response()->json($resposta);
            }

            if ($countCategoriasPai < 50) {
                // Categorias filhas
                // DB::enableQueryLog();
                $categoriasFilhas = $eComPlusService->obterCategoriasFilhas();
                // return DB::getQueryLog();

                if (count($categoriasFilhas) == 0) {
                    // $eComPlusService->gerarLog('Sem categorias atualizadas para envio.');
                    // return response()->json('Sem categorias para atualização.');
                    return 'sem categorias';
                }

                $nameParent = '';
                $slugParent = '';

                foreach ($categoriasFilhas as $categoriaFilha) {
                    $categoriaEnviada = false;
                    $name             = converterTextoCp1252ToUtf8($categoriaFilha->NAME);
                    $slug             = utf8_encode($categoriaFilha->NAME);
                    $slug             = formatarSlug($slug);

                    $nameParent = converterTextoCp1252ToUtf8($categoriaFilha->NAME_PAI);
                    $slugParent = utf8_encode($categoriaFilha->NAME_PAI);
                    $slugParent = formatarSlug($slugParent);

                    // return response()->json($categoriaFilha->ID);

                    $dadosCategoriaFilha = [
                        'name'   => $name,
                        'slug'   => $slug,
                        'parent' => [
                            '_id'  => $categoriaFilha->E_COM_PLUS_ID_PAI,
                            'name' => $nameParent,
                            'slug' => $slugParent,
                        ],
                    ];

                    if (is_null($categoriaFilha->E_COM_PLUS_ID)) {
                        try {
                            // return response()->json($dadosCategoriaFilha);

                            $countCategoriasFilhas = EGrupos::select(DB::raw('COUNT(GRUP_E_COM_PLUS_ID)'))->whereNotNull('GRUP_E_COM_PLUS_ID')->get();

                            $countCategoriasFilhas = $countCategoriasFilhas[0]->COUNT;

                            if ($countCategoriasFilhas >= 50) {
                                $eComPlusService->gerarLog('Não é possível enviar mais de 50 categorias.');
                                break;
                            }

                            $resposta         = json_decode($eComPlusService->criarCategoria($dadosCategoriaFilha), true);
                            $categoriaEnviada = true;
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Categoria filha ' . $name . ' não foi enviada. Detalhes: ' . $th->getMessage());
                            continue;
                        }

                        try {
                            if (!is_null($resposta) || count($resposta) > 0) {
                                $eComPlusService->atualizarIdEComPlusCategoriaFilhaEnviada($categoriaFilha->ID, $resposta['_id']);
                                $resposta = 'Categoria filha' . $categoriaFilha->ID . ' - ' . $name . ', slug: ' . $slug . ' enviada. Novo _id: ' . $resposta['_id'];
                            }
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('SBGR_E_COM_PLUS_ID da categoria filha ' . $name . ', slug: ' . $slug . ' não foi salvo. Detalhes: ' . $th->getMessage());
                        }
                    } else {
                        try {
                            // return response()->json($dadosCategoriaFilha);
                            $eComPlusService->editarCategoria($categoriaFilha->E_COM_PLUS_ID, $dadosCategoriaFilha);
                            $categoriaEnviada = true;
                            $resposta         = 'Categoria ' . $categoriaFilha->ID . ' - ' . $name . ', slug: ' . $slug . ' atualizada.';
                        } catch (\Throwable $th) {
                            $eComPlusService->gerarLog('Categoria filha ' . $name . ', slug: ' . $slug . ' não foi atualizada. Detalhes: ' . $th->getMessage());
                        }
                    }

                    $eComPlusService->gerarLog($resposta);

                    // return response()->json($resposta);
                }
            }

            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto

            if ($categoriaEnviada) {
                return response()->json('Categorias enviadas e/ou atualizadas.');
                return response()->json('Categorias enviadas e/ou atualizadas.');
            } else {
                return response()->json('Nenhuma categoria enviada e/ou atualizada.');
                return response()->json('Nenhuma categoria enviada e/ou atualizada.');
            }
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao enviar/atualizar categoria(s). Detalhes: ' . $th->getMessage());
            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
            return response()->json('Erro ao enviar/atualizar categoria(s). Detalhes: ' . $th->getMessage());
        }
    }
}

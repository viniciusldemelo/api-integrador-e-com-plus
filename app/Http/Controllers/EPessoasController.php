<?php

namespace App\Http\Controllers;

use App\Library\Services\EComPlusService;

class EPessoasController extends Controller
{
    private $eComPlusService;

    public function __construct()
    {
        $this->eComPlusService = new EComPlusService();
    }

    /**
     * Pesquisar cliente por e-mail
     *
     * @param  int  $email
     * @return \Illuminate\Http\Response
     */
    public function pesquisarCliente($email)
    {
        return $this->eComPlusService->pesquisarCliente($email);
    }
}

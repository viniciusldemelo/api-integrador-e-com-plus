<?php

namespace App\Console\Commands;

use App\Http\Controllers\EDocumentosAuxiliaresVendasController;
use App\Http\Controllers\EGruposController;
use App\Http\Controllers\EMarcasController;
use App\Http\Controllers\EProdutosController;
use App\Library\Services\EComPlusService;
use Illuminate\Console\Command;

class SincronizacaoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sincronizacao:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronização de dados com e-commerce e ERP Estação Sistemas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $eComPlusService = new EComPlusService();

        $eComPlusService::gerarLog('Sincronização iniciada.');

        EMarcasController::enviarImagensMarcas();
        EProdutosController::enviarImagensProdutos();
        EGruposController::enviarCategorias();
        EMarcasController::enviarMarcas();
        EProdutosController::enviarProdutos();
        EDocumentosAuxiliaresVendasController::importarPedidos();

        $eComPlusService::gerarLog('Sincronização finalizada.');
    }
}

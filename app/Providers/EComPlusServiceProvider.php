<?php

namespace App\Providers;

use App\Library\Services\EComPlusService;
use Illuminate\Support\ServiceProvider;

class EComPlusServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Library\Services\EComPlus', function ($app) {
            return new EComPlusService();
        });
    }
}

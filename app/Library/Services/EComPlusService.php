<?php

namespace App\Library\Services;

use App\Http\Controllers\EConfiguracoesSistemasController;
use App\Models\EArquivosECommerce;
use App\Models\EDocumentosAuxiliaresItens;
use App\Models\EDocumentosAuxiliaresPagar;
use App\Models\EDocumentosAuxiliaresVendas;
use App\Models\EGrupos;
use App\Models\ELogs;
use App\Models\EMarcas;
use App\Models\EPessoas;
use App\Models\EProdutos;
use App\Models\ESubGrupos;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PDO;

class EComPlusService
{

    /**
     * Obter novo DCAV_NUMERO
     *
     * @return \Illuminate\Support\Collection
     */
    public function obterNovoDcavNumero()
    {
        $novoNumero = EDocumentosAuxiliaresVendas::select(DB::raw('MAX(DCAV_NUMERO) AS DCAV_NUMERO_ATUAL'))->get()->first();
        $novoNumero = $novoNumero->DCAV_NUMERO_ATUAL + 1;

        return $novoNumero;
    }

    public function gerarAssinaturaRegistro($dataAtualizacao, $nomeUsuario, $nomeEstacao, $nomeEstacaoConta)
    {
        return Hash::make($dataAtualizacao . '_' . $nomeUsuario . '_' . $nomeEstacao . '_' . $nomeEstacaoConta);
    }

    /**
     * Obter produtos base ERP
     *
     * @return \Illuminate\Support\Collection
     */
    public function obterProdutos()
    {
        try {
            $eComPlusService = new EComPlusService();

            $dataHoraCincoMinutosAtras = Carbon::now()->subMinutes(5);
            $dataHoraCincoMinutosAtras = $dataHoraCincoMinutosAtras->toDateTimeString();

            $dataHoraAgora = Carbon::now();
            $dataHoraAgora = $dataHoraAgora->toDateTimeString();

            $prunId = DB::select('
                SELECT DISTINCT
                    PRUN_ID
                FROM E_ESTOQUE ESTQ
                    INNER JOIN E_CONFIGURACOES_SISTEMAS CFST ON
                    ESTQ.UNEM_ID = CFST.UNEM_ID
                WHERE
                    ESTQ.ESTQ_ATUALIZACAO BETWEEN ? AND ?
                AND CFST.CFST_E_COM_PLUS_STORE_ID = ?',
                [
                    $dataHoraCincoMinutosAtras,
                    $dataHoraAgora,
                    env('E_COM_PLUS_STORE_ID', 0),
                ]);

            if (count($prunId) == 0) {
                return [
                    'mensagem' => 'Sem dados',
                ];
            }

            $inPrunId = '';

            foreach ($prunId as $produto) {
                if (count($prunId) == 1) {
                    $inPrunId .= $produto->PRUN_ID;
                } else {
                    $inPrunId .= $produto->PRUN_ID . ',';
                }
            }

            if ($inPrunId[strlen($inPrunId) - 1] == ',') {
                $inPrunId = substr($inPrunId, 0, strlen($inPrunId) - 1);
            }

            // return env('E_COM_PLUS_STORE_ID', 0);
            // return $inPrunId;

            $sqlV_PRODUTOS_E_COMMERCE = '
                SELECT
                    *
                FROM V_PRODUTOS_E_COMMERCE VPEC
                WHERE VPEC.STORE_ID = ' . env('E_COM_PLUS_STORE_ID', 0) . '
                AND
                    VPEC.PRUN_ID IN (' . $inPrunId . ')
                AND
                    VPEC.PRICE > 0
                AND
                    VPEC.NAME IS NOT NULL
                ORDER BY
                    VPEC.SKU';

            // return $sqlV_PRODUTOS_E_COMMERCE;

            // Conexão Firebird por PDO. Com kkszymanowski/laravel-6-firebird versão 6.* não é possível pesquisar com IN() gigante.
            $pdoConexaoFirebird = new PDO('firebird:dbname=' . env('DB_HOST', '') . ':' . env('DB_DATABASE', '') . ';charset=utf8',
                env('DB_USERNAME', ''),
                env('DB_PASSWORD', ''),
                [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                ]
            );

            $query = $pdoConexaoFirebird->query($sqlV_PRODUTOS_E_COMMERCE);
            $query->execute();

            $registros = $query->fetchAll(PDO::FETCH_OBJ);

            $produtos = [];

            foreach ($registros as $registro) {
                array_push($produtos, [
                    'UNEM_ID'              => $registro->UNEM_ID,
                    'PROD_ID'              => $registro->PROD_ID,
                    'PRMC_ID'              => $registro->PRMC_ID,
                    'PRGD_ID'              => $registro->PRGD_ID,
                    'PRUN_ID'              => $registro->PRUN_ID,
                    'MARC_ID'              => $registro->MARC_ID,
                    'E_COM_PLUS_ID'        => $registro->E_COM_PLUS_ID,
                    'STORE_ID'             => $registro->STORE_ID,
                    'SKU'                  => $registro->SKU,
                    'NAME'                 => $registro->NAME,
                    'SLUG'                 => $registro->SLUG,
                    'SHORT_DESCRIPTION'    => $registro->SHORT_DESCRIPTION,
                    'BODY_HTML'            => $registro->BODY_HTML,
                    'BODY_TEXT'            => $registro->BODY_TEXT,
                    'META_TITLE'           => $registro->META_TITLE,
                    'META_DESCRIPTION'     => $registro->META_DESCRIPTION,
                    'KEYWORDS'             => $registro->KEYWORDS,
                    'PRICE_EFFECTIVE_DATE' => $registro->PRICE_EFFECTIVE_DATE,
                    'WEIGHT'               => $registro->WEIGHT,
                    'GTIN'                 => $registro->GTIN,
                    'PRICE'                => $registro->PRICE,
                    'BASE_PRICE'           => $registro->BASE_PRICE,
                    'QUANTITY'             => $registro->QUANTITY,
                    'LOGO_URL_MARCA'       => $registro->LOGO_URL_MARCA,
                    'DATA_ATUALIZACAO'     => $registro->DATA_ATUALIZACAO,
                ]);
            }

            return $produtos;
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao obter produtos. Detalhes: ' . $th->getMessage());
            return 'Erro ao obter produtos. Detalhes: ' . $th->getMessage();
        }
    }

    /**
     * Obter dimensões por produto
     *
     * @param int $sku
     * @return \Illuminate\Support\Collection
     */
    public function obterDimensoesPorProduto($sku)
    {
        return DB::table('E_PRODUTOS_UNIDADES AS PRUN')->join('E_PRODUTOS AS PROD', 'PRUN.PROD_ID', '=', 'PROD.PROD_ID')
            ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_UNIDADES_MEDIDAS AS UNMD', 'PRUN.UNMD_ID', '=', 'UNMD.UNMD_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('PRUN.PRUN_EMBALAGEM_ALTURA AS HEIGHT', 'PRUN.PRUN_EMBALAGEM_PROFUNDIDADE AS LENGTH', 'PRUN.PRUN_EMBALAGEM_LARGURA AS WIDTH', 'PRUN.PRUN_PESO_BRUTO AS WEIGHT', DB::raw('UPPER(UNMD.UNMD_SIGLA) AS UNIT'))
            ->where('PRUN.PRUN_CODIGO', $sku)
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->get();
    }

    /**
     * Obter marcas por produto
     *
     * @param int $sku
     * @return \Illuminate\Support\Collection
     */
    public function obterMarcasPorProduto($sku)
    {
        return DB::table('E_PRODUTOS_MARCAS AS PRMC')->join('E_PRODUTOS AS PROD', 'PRMC.PROD_ID', '=', 'PROD.PROD_ID')
            ->join('E_PRODUTOS_UNIDADES AS PRUN', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_MARCAS AS MARC', 'PRMC.MARC_ID', '=', 'MARC.MARC_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('MARC.MARC_ID AS ID', 'MARC.MARC_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'MARC.MARC_NOME AS NAME', 'MARC.MARC_LOGO_URL AS LOGO_URL', 'MARC.MARC_LOGO_TAMANHO AS LOGO_SIZE')
            ->where('PRUN.PRUN_CODIGO', $sku)
            ->whereNotNull('MARC.MARC_LOGO_URL')
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->get();
    }

    /**
     * Obter categorias por produto
     *
     * @param int $sku
     * @return \Illuminate\Support\Collection
     */
    public function obterCategoriasPorProduto($sku)
    {
        $categoria = DB::table('E_PRODUTOS AS PROD')
            ->join('E_PRODUTOS_UNIDADES AS PRUN', 'PROD.PROD_ID', '=', 'PRUN.PROD_ID')
            ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_GRUPOS AS GRUP', 'PROD.GRUP_ID', '=', 'GRUP.GRUP_ID')
            ->join('E_SUB_GRUPOS AS SBGR', 'PROD.SBGR_ID', '=', 'SBGR.SBGR_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('GRUP.GRUP_ID AS ID_PAI', 'GRUP.GRUP_E_COM_PLUS_ID AS E_COM_PLUS_ID_PAI', 'GRUP.GRUP_NOME AS NAME_PAI', 'SBGR.SBGR_ID AS ID', 'SBGR.SBGR_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'SBGR.SBGR_NOME AS NAME')
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->where('PRUN.PRUN_CODIGO', $sku)
            ->whereNotNull('GRUP.GRUP_E_COM_PLUS_ID')
            ->orderBy('GRUP.GRUP_ID')
            ->orderBy('SBGR.SBGR_ID')
            ->distinct()
            ->get();

        if (count($categoria) > 0 && $categoria[0]->ID == 1) {
            $categoria = DB::table('E_PRODUTOS AS PROD')
                ->join('E_PRODUTOS_UNIDADES AS PRUN', 'PROD.PROD_ID', '=', 'PRUN.PROD_ID')
                ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
                ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
                ->join('E_GRUPOS AS GRUP', 'PROD.GRUP_ID', '=', 'GRUP.GRUP_ID')
                ->join('E_SUB_GRUPOS AS SBGR', 'PROD.SBGR_ID', '=', 'SBGR.SBGR_ID')
                ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
                ->select('GRUP.GRUP_ID AS ID', 'GRUP.GRUP_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'GRUP.GRUP_NOME AS NAME')
                ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
                ->where('SBGR.SBGR_ID', '>', 1)
                ->where('PRUN.PRUN_CODIGO', $sku)
                ->whereNotNull('GRUP.GRUP_E_COM_PLUS_ID')
                ->orderBy('GRUP.GRUP_ID')
                ->orderBy('SBGR.SBGR_ID')
                ->distinct()
                ->get();
        }

        return $categoria;
    }

    /**
     * Obter imagens por produto
     *
     * @param int $sku
     * @return \Illuminate\Support\Collection
     */
    public function obterImagensPorProduto($sku)
    {
        return DB::table('E_ARQUIVOS_E_COMMERCE AS AREC')->join('E_PRODUTOS AS PROD', 'AREC.PROD_ID', '=', 'PROD.PROD_ID')
            ->join('E_PRODUTOS_MARCAS AS PRMC', 'AREC.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'AREC.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_PRODUTOS_UNIDADES AS PRUN', 'AREC.PRUN_ID', '=', 'PRUN.PRUN_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('AREC.AREC_ID AS ID', 'AREC.AREC_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'AREC.AREC_URL_ANEXO_TITULO AS TITULO_IMAGEM', 'AREC.AREC_URL_ANEXO AS URL_IMAGEM')
            ->where('PRUN.PRUN_CODIGO', $sku)
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->whereNotNull('AREC.AREC_URL_ANEXO')
            ->get();
    }

    /**
     * Obter categorias pai
     *
     * @return \Illuminate\Support\Collection
     */
    public function obterCategoriasPai()
    {
        $dataHoraCincoMinutosAtras = Carbon::now()->subMinutes(5);
        $dataHoraCincoMinutosAtras = $dataHoraCincoMinutosAtras->toDateTimeString();

        $dataHoraAgora = Carbon::now();
        $dataHoraAgora = $dataHoraAgora->toDateTimeString();

        return DB::table('E_GRUPOS AS GRUP')->join('E_PRODUTOS AS PROD', 'GRUP.GRUP_ID', '=', 'PROD.GRUP_ID')
            ->join('E_PRODUTOS_UNIDADES AS PRUN', 'PROD.PROD_ID', '=', 'PRUN.PROD_ID')
            ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('GRUP.GRUP_ID AS ID', 'GRUP.GRUP_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'GRUP.GRUP_NOME AS NAME')
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->whereBetween('GRUP.GRUP_ATUALIZACAO', [
                $dataHoraCincoMinutosAtras,
                $dataHoraAgora,
            ])
        // ->where('GRUP.GRUP_ID', 21)
            ->orderBy('GRUP.GRUP_ID')
            ->distinct()
            ->get();
    }

    /**
     * Obter categorias pai
     *
     * @return \Illuminate\Support\Collection
     */
    public function obterCategoriasFilhas()
    {
        return DB::table('E_PRODUTOS AS PROD')->join('E_PRODUTOS_UNIDADES AS PRUN', 'PROD.PROD_ID', '=', 'PRUN.PROD_ID')
            ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_GRUPOS AS GRUP', 'PROD.GRUP_ID', '=', 'GRUP.GRUP_ID')
            ->join('E_SUB_GRUPOS AS SBGR', 'PROD.SBGR_ID', '=', 'SBGR.SBGR_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('GRUP.GRUP_ID AS ID_PAI', 'GRUP.GRUP_E_COM_PLUS_ID AS E_COM_PLUS_ID_PAI', 'GRUP.GRUP_NOME AS NAME_PAI', 'SBGR.SBGR_ID AS ID', 'SBGR.SBGR_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'SBGR.SBGR_NOME AS NAME')
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->where('SBGR.SBGR_ID', '>', 1)
            ->whereNotNull('GRUP.GRUP_E_COM_PLUS_ID')
            ->orderBy('GRUP.GRUP_ID')
            ->orderBy('SBGR.SBGR_ID')
            ->distinct()
            ->get();
    }

    /**
     * Obter marcas
     *
     * @return \Illuminate\Support\Collection
     */
    public function obterMarcas()
    {
        $dataHoraCincoMinutosAtras = Carbon::now()->subMinutes(5);
        $dataHoraCincoMinutosAtras = $dataHoraCincoMinutosAtras->toDateTimeString();

        $dataHoraAgora = Carbon::now();
        $dataHoraAgora = $dataHoraAgora->toDateTimeString();

        return DB::table('E_PRODUTOS_MARCAS AS PRMC')->join('E_PRODUTOS AS PROD', 'PRMC.PROD_ID', '=', 'PROD.PROD_ID')
            ->join('E_MARCAS AS MARC', 'PRMC.MARC_ID', '=', 'MARC.MARC_ID')
            ->join('E_PRODUTOS_UNIDADES AS PRUN', 'PROD.PROD_ID', '=', 'PRUN.PROD_ID')
            ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
            ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
            ->select('MARC.MARC_ID AS ID', 'MARC.MARC_E_COM_PLUS_ID AS E_COM_PLUS_ID', 'MARC.MARC_NOME AS NAME', 'MARC.MARC_LOGO_URL AS LOGO_URL', 'MARC.MARC_LOGO_TAMANHO AS LOGO_SIZE')
            ->where('CFST.CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
            ->whereNotNull('MARC.MARC_LOGO_URL')
            ->whereBetween('MARC.MARC_ATUALIZACAO', [
                $dataHoraCincoMinutosAtras,
                $dataHoraAgora,
            ])
            ->orderBy('MARC.MARC_ID')
            ->distinct()
            ->get();
    }

    /**
     * Atualizar PROD_E_COM_PLUS_ID de produto enviado
     *
     * @param string $sku
     * @param string $eComPlusId
     *            = id do produto no e-commerce
     * @return \Illuminate\Support\Collection
     */
    public function atualizarIdEComPlusProdutoEnviado($sku, $eComPlusId)
    {
        try {
            $eComPlusService = new EComPlusService();

            $produto = DB::select('
                SELECT PRUN.PROD_ID FROM E_PRODUTOS_UNIDADES AS PRUN WHERE PRUN.PRUN_CODIGO = ?', [
                $sku,
            ]);

            // return $eComPlusId;

            $eProdutos                     = EProdutos::find($produto[0]->PROD_ID);
            $eProdutos->PROD_E_COM_PLUS_ID = $eComPlusId;
            $eProdutos->save();

        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao atualizar PROD_E_COM_PLUS_ID do produto ' . $sku . '. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Atualizar GRUP_E_COM_PLUS_ID de categoria enviada
     *
     * @param string $id
     * @param string $eComPlusId
     * @return \Illuminate\Support\Collection
     */
    public function atualizarIdEComPlusCategoriaEnviada($id, $eComPlusId)
    {
        try {
            $eComPlusService = new EComPlusService();

            $eGrupos                     = EGrupos::find($id);
            $eGrupos->GRUP_E_COM_PLUS_ID = $eComPlusId;
            $eGrupos->save();
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao atualizar GRUP_E_COM_PLUS_ID da categoria ' . $id . '. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Atualizar SBGR_E_COM_PLUS_ID de categoria enviada
     *
     * @param string $id
     * @param string $eComPlusId
     * @return \Illuminate\Support\Collection
     */
    public function atualizarIdEComPlusCategoriaFilhaEnviada($id, $eComPlusId)
    {
        try {
            $eComPlusService = new EComPlusService();

            $eSubGrupos                     = ESubGrupos::find($id);
            $eSubGrupos->SBGR_E_COM_PLUS_ID = $eComPlusId;
            $eSubGrupos->save();
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao atualizar SBGR_E_COM_PLUS_ID da subcategoria ' . $id . '. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Atualizar MARC_E_COM_PLUS_ID de marca enviada
     *
     * @param string $id
     * @param string $eComPlusId
     * @return \Illuminate\Support\Collection
     */
    public function atualizarIdEComPlusMarcaEnviada($id, $eComPlusId)
    {
        try {
            $eComPlusService = new EComPlusService();

            $eMarcas                     = EMarcas::find($id);
            $eMarcas->MARC_E_COM_PLUS_ID = $eComPlusId;
            $eMarcas->save();
        } catch (\Throwable $th) {
            $eComPlusService->gerarLog('Erro ao atualizar MARC_E_COM_PLUS_ID da marca ' . $id . '. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Gerar novo log
     *
     * @param string $descricao
     * @return \Illuminate\Support\Collection
     */
    public static function gerarLog($descricao)
    {
        $dataHoraAgoraLog = Carbon::now();
        $dataHoraAgoraLog = $dataHoraAgoraLog->toDateTimeString();

        $eLogs = new ELogs();

        $eLogs->LOGS_NOME      = env('NOME_ESTACAO_USUARIO', '');
        $eLogs->LOGS_DESCRICAO = converterTextoParaWindows1252($descricao);
        $eLogs->LOGS_DATA_HORA = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraAgoraLog);

        $eLogs->save();

        // laravel.log
        Log::info($descricao);
    }

    /* Requisições Guzzle */

    /**
     * Gerar novo token de acesso
     *
     * @param int $id
     * @param int $apiKey
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function gerarNovoTokenAcesso($id, $apiKey)
    {
        try {
            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type' => 'application/json',
                    'X-Store-ID'   => env('E_COM_PLUS_STORE_ID', 0),
                ],
            ]);

            $body = [
                "_id"     => $id,
                "api_key" => $apiKey,
            ];

            $response = $client->request('POST', '_authenticate.json', [
                'body' => json_encode($body),
            ]);

            return $response->getBody();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /* Produtos */

    /**
     * Listar produtos
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listarProdutos()
    {
        try {
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type' => 'application/json',
                    'X-Store-ID'   => env('E_COM_PLUS_STORE_ID', 0),
                ],
            ]);

            $request  = new Request('GET', 'products.json');
            $response = $client->send($request);

            return $response->getBody();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Encontrar produto
     *
     * @param string $sku
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function encontrarProduto($sku)
    {
        try {
            // return response()->json($sku);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type' => 'application/json',
                    'X-Store-ID'   => env('E_COM_PLUS_STORE_ID', 0),
                ],
            ]);

            $request  = new Request('GET', 'products.json?sku=' . $sku);
            $response = $client->send($request);

            return $response->getBody();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Criar produto
     *
     * @param array $dadosProduto
     * @return string
     */
    public function criarProduto($dadosProduto)
    {
        try {
            // return $dadosProduto;
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('POST', 'products.json', [
                'body' => json_encode($dadosProduto),
            ]);

            return $response->getBody()->getContents();
            // return $response;
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Editar produto
     *
     * @param string $_id
     *            = id do produto no e-commerce
     * @param array $dadosProduto
     * @return string
     */
    public function editarProduto($_id, $dadosProduto)
    {
        try {
            // return response()->json($dadosProduto);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('PATCH', 'products/' . $_id . '.json', [
                'body' => json_encode($dadosProduto),
            ]);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /* Categorias */

    /**
     * Listar categorias
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listarCategorias()
    {
        try {
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type' => 'application/json',
                    'X-Store-ID'   => env('E_COM_PLUS_STORE_ID', 0),
                ],
            ]);

            $request  = new Request('GET', 'categories.json');
            $response = $client->send($request);

            return $response->getBody();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Encontrar categoria
     *
     * @param string $slug
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function encontrarCategoria($slug)
    {
        try {
            // return response()->json($slug);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type' => 'application/json',
                    'X-Store-ID'   => env('E_COM_PLUS_STORE_ID', 0),
                ],
            ]);

            $request  = new Request('GET', 'categories.json?slug=' . $slug);
            $response = $client->send($request);

            return $response->getBody();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Criar categoria
     *
     * @param array $dadosCategoria
     * @return string
     */
    public function criarCategoria($dadosCategoria)
    {
        try {
            // return response()->json($dadosCategoria);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('POST', 'categories.json', [
                'body' => json_encode($dadosCategoria),
            ]);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Editar categoria
     *
     * @param string $_id
     *            = id da categoria no e-commerce
     * @param array $dadosCategoria
     * @return string
     */
    public function editarCategoria($_id, $dadosCategoria)
    {
        try {
            // return response()->json($dadosCategoria);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('PATCH', 'categories/' . $_id . '.json', [
                'body' => json_encode($dadosCategoria),
            ]);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Criar marca
     *
     * @param array $dadosMarca
     * @return string
     */
    public function criarMarca($dadosMarca)
    {
        try {
            // return response()->json($dadosMarca);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('POST', 'brands.json', [
                'body' => json_encode($dadosMarca),
            ]);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    /**
     * Editar marca
     *
     * @param string $_id
     *            = id da marca no e-commerce
     * @param array $dadosMarca
     * @return string
     */
    public function editarMarca($_id, $dadosMarca)
    {
        try {
            // return response()->json($dadosMarca);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $response = $client->request('PATCH', 'brands/' . $_id . '.json', [
                'body' => json_encode($dadosMarca),
            ]);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    // Cliente

    /**
     * Pesquisar cliente
     *
     * @param string $email
     * @return \Psr\Http\Message\MessageInterface
     */
    public function pesquisarCliente($email)
    {
        try {
            // return response()->json($email);
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $request  = new Request('GET', 'customers.json?main_email=' . $email . '&fields=doc_number,phones');
            $response = $client->send($request);

            return $response->getBody()->getContents();
        } catch (ClientException $e) {
            // echo Psr7\Message::toString($e->getRequest());
            return Psr7\Message::toString($e->getResponse());
        }
    }

    // Pedido

    /**
     * Pesquisar todos pedidos e-commerce
     *
     * @return \Psr\Http\Message\MessageInterface
     */
    public function pesquisarTodosPedidos()
    {
        try {
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            // return $validacao;

            $client = new Client([
                'base_uri' => obterBaseUriEcomPlus(),
                'timeout'  => 2000,
                'headers'  => [
                    'Content-Type'   => 'application/json',
                    'X-Store-ID'     => env('E_COM_PLUS_STORE_ID', 0),
                    'X-Access-Token' => $validacao['X-Access-Token'],
                    'X-My-ID'        => $configSistema['CFST_E_COM_PLUS_ID'],
                ],
            ]);

            $request  = new Request('GET', 'orders.json?sort=-updated_at&fields=_id,number,buyers,items,amount,shipping_method_label,payment_method_label,financial_status,notes,created_at,updated_at');
            $response = $client->send($request);

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao pesquisar todos os pedidos. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Salvar todos pedidos e-commerce
     *
     * @param array $pedidos
     * @return \Psr\Http\Message\MessageInterface
     */
    public function salvarPedido($pedidos)
    {
        ini_set('max_execution_time', '600'); // 600 segundos == 10 minutos

        try {
            $configSistema = EConfiguracoesSistemasController::obterConfiguracaoSistema();

            $formaPagamentoId   = 0;
            $pedidosImportados  = 0;
            $pedidosAtualizados = 0;
            $pedidoConsistente  = true;

            foreach ($pedidos as $pedido) {
                $eDocumentosAuxiliaresVendas = EDocumentosAuxiliaresVendas::select('*')->where('DCAV_E_COM_PLUS_ID', $pedido['_id'])->get()->first();

                if (!isset($eDocumentosAuxiliaresVendas)) {
                    /* Variáveis */
                    $dataHoraHoje = Carbon::now();
                    $dataHoraHoje = $dataHoraHoje->toDateTimeString();

                    $dataEmissaoPedido     = date('Y-m-d H:i:s', strtotime($pedido['created_at']));
                    $dataAtualizacaoPedido = date('Y-m-d H:i:s', strtotime($pedido['updated_at']));
                    $dataReferencia        = date('Y-m-d', strtotime($pedido['created_at']));

                    $dataVencimento = Carbon::now();
                    $dataVencimento->addDays(7);
                    $dataVencimento = $dataVencimento->toDateTimeString();
                    $dataVencimento = date('Y-m-d', strtotime($dataVencimento));

                    $dataPrazoMedioEntrega = Carbon::now();
                    $dataPrazoMedioEntrega->addDays(15);
                    $dataPrazoMedioEntrega = $dataPrazoMedioEntrega->toDateTimeString();
                    $dataPrazoMedioEntrega = date('Y-m-d', strtotime($dataPrazoMedioEntrega));

                    /* Cliente */
                    try {
                        foreach ($pedido['clientes'] as $clientePedido) {
                            $cliente = $this->pesquisarCliente($clientePedido['main_email']);

                            $cliente = json_decode(strval($cliente), true);
                            $cliente = $cliente['result'];

                            $ePessoas = EPessoas::select('PESS_E_COM_PLUS_ID', 'PESS_EMAIL', 'PESS_NOME_RAZAO_SOCIAL', 'PESS_ATUALIZACAO', 'PESS_ASSINATURA')->where('PESS_CNPJ_CPF', $cliente[0]['doc_number'])->get()->first();

                            if (!isset($ePessoas)) {
                                $ePessoas = new EPessoas();

                                $ePessoas->CPRC_ID                = $configSistema['CPRC_ID'];
                                $ePessoas->EMPR_ID                = $configSistema['EMPR_ID'];
                                $ePessoas->UNEM_ID                = $configSistema['UNEM_ID'];
                                $ePessoas->PESS_E_COM_PLUS_ID     = $cliente[0]['_id'];
                                $ePessoas->PESS_EMAIL             = $clientePedido['main_email'];
                                $ePessoas->PESS_APELIDO_FANTASIA  = converterTextoParaWindows1252($clientePedido['name']['given_name'] . ' ' . $clientePedido['name']['family_name']);
                                $ePessoas->PESS_NOME_RAZAO_SOCIAL = converterTextoParaWindows1252($clientePedido['name']['given_name'] . ' ' . $clientePedido['name']['family_name']);
                                $ePessoas->PESS_CNPJ_CPF          = $cliente[0]['doc_number'];
                                $ePessoas->PESS_DATA_CADASTRO     = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje);
                                $ePessoas->PESS_ATUALIZACAO       = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje);
                                $ePessoas->PESS_USUARIO           = env('NOME_ESTACAO_USUARIO', '');
                                $ePessoas->PESS_ESTACAO           = env('NOME_ESTACAO_USUARIO', '');
                                $ePessoas->PESS_ESTACAO_CONTA     = env('NOME_ESTACAO_USUARIO', '');
                                $ePessoas->PESS_ASSINATURA        = $this->gerarAssinaturaRegistro($ePessoas->PESS_ATUALIZACAO, $ePessoas->PESS_USUARIO, $ePessoas->PESS_ESTACAO, $ePessoas->PESS_ESTACAO_CONTA);

                                $ePessoas->save();
                            } else {
                                EPessoas::where('PESS_CNPJ_CPF', $cliente[0]['doc_number'])
                                    ->update(
                                        [
                                            'PESS_E_COM_PLUS_ID'     => $cliente[0]['_id'],
                                            'PESS_EMAIL'             => $clientePedido['main_email'],
                                            'PESS_APELIDO_FANTASIA'  => converterTextoParaWindows1252($clientePedido['name']['given_name'] . ' ' . $clientePedido['name']['family_name']),
                                            'PESS_NOME_RAZAO_SOCIAL' => converterTextoParaWindows1252($clientePedido['name']['given_name'] . ' ' . $clientePedido['name']['family_name']),
                                            'PESS_ATUALIZACAO'       => Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje),
                                            'PESS_ASSINATURA'        => $this->gerarAssinaturaRegistro($ePessoas->PESS_ATUALIZACAO, $ePessoas->PESS_USUARIO, $ePessoas->PESS_ESTACAO, $ePessoas->PESS_ESTACAO_CONTA),
                                        ]
                                    );
                            }
                        }
                    } catch (\Throwable $th) {
                        $this->gerarLog('Cliente(s) do pedido ' . $pedido['_id'] . ' não foi/foram importado(s)/atualizado(s). Detalhes: ' . $th->getMessage());
                        $pedidoConsistente = false;
                    }

                    /* Capa do pedido */
                    try {
                        /* Status financeiro */
                        $statusFinanceiro = '';

                        switch ($pedido['status_financeiro']) {
                            case 'pending':
                                $statusFinanceiro = 'Pendente';
                                break;

                            case 'under_analysis':
                                $statusFinanceiro = 'Sob análise';
                                break;

                            case 'authorized':
                                $statusFinanceiro = 'Autorizado';
                                break;

                            case 'unauthorized':
                                $statusFinanceiro = 'Não autorizado';
                                break;

                            case 'partially_paid':
                                $statusFinanceiro = 'Parcialmente pago';
                                break;

                            case 'paid':
                                $statusFinanceiro = 'Pago';
                                break;

                            case 'in_dispute':
                                $statusFinanceiro = 'Em disputa';
                                break;

                            case 'partially_refunded':
                                $statusFinanceiro = 'Parcialmente ressarcido';
                                break;

                            case 'refunded':
                                $statusFinanceiro = 'Ressarcido';
                                break;

                            case 'voided':
                                $statusFinanceiro = 'Cancelado';
                                break;

                            case 'unknown':
                                $statusFinanceiro = 'Desconhecido';
                                break;
                        }

                        /* Cliente Pedido */
                        $ePessoas = EPessoas::select('PESS_ID')->where('PESS_CNPJ_CPF', $cliente[0]['doc_number'])->get()->first();

                        $eDocumentosAuxiliaresVendas = new EDocumentosAuxiliaresVendas();

                        $eDocumentosAuxiliaresVendas->CPRC_ID                         = $configSistema['CPRC_ID'];
                        $eDocumentosAuxiliaresVendas->EMPR_ID                         = $configSistema['EMPR_ID'];
                        $eDocumentosAuxiliaresVendas->UNEM_ID                         = $configSistema['UNEM_ID'];
                        $eDocumentosAuxiliaresVendas->USRS_ID                         = env('USRS_ID', 0);
                        $eDocumentosAuxiliaresVendas->PESS_ID                         = env('PESS_ID', 0);
                        $eDocumentosAuxiliaresVendas->PAIS_ID                         = 1058;
                        $eDocumentosAuxiliaresVendas->PESS_INDI_ID                    = $ePessoas->PESS_ID;
                        $eDocumentosAuxiliaresVendas->DCAV_NOME                       = converterTextoParaWindows1252($clientePedido['name']['given_name'] . ' ' . $clientePedido['name']['family_name']);
                        $eDocumentosAuxiliaresVendas->DCAV_EMAIL                      = $clientePedido['main_email'];
                        $eDocumentosAuxiliaresVendas->DCAV_CPF_CNPJ                   = $cliente[0]['doc_number'];
                        $eDocumentosAuxiliaresVendas->DCAV_TELEFONE                   = $cliente[0]['phones'][0]['number'];
                        $eDocumentosAuxiliaresVendas->DCAV_NUMERO                     = $pedido['numero_pedido'];
                        $eDocumentosAuxiliaresVendas->DCAV_VALOR_FRETE                = floatval($pedido['montante']['freight']);
                        $eDocumentosAuxiliaresVendas->DCAV_VALOR_DESCONTO             = floatval($pedido['montante']['discount']);
                        $eDocumentosAuxiliaresVendas->DCAV_DATA_EMISSAO               = $dataEmissaoPedido;
                        $eDocumentosAuxiliaresVendas->DCAV_STATUS_FINANCEIRO_E_COMMER = $statusFinanceiro;
                        $eDocumentosAuxiliaresVendas->DCAV_CONTROLE                   = $pedido['numero_pedido'];
                        $eDocumentosAuxiliaresVendas->DCAV_ORIGEM                     = 'E-commerce e-com.plus';
                        $eDocumentosAuxiliaresVendas->DCAV_OBSERVACAO                 = converterTextoParaWindows1252($pedido['notas_pedido'] . ' Status financeiro: ' . $statusFinanceiro);
                        $eDocumentosAuxiliaresVendas->DCAV_OBSERVACAO_NOTA            = converterTextoParaWindows1252('ID Pedido e-com.plus: ' . $pedido['_id'] . ' Nº pedido: ' . $pedido['numero_pedido']);
                        $eDocumentosAuxiliaresVendas->DCAV_E_COM_PLUS_ID              = $pedido['_id'];
                        $eDocumentosAuxiliaresVendas->DCAV_ATUALIZACAO                = $dataAtualizacaoPedido;
                        $eDocumentosAuxiliaresVendas->DCAV_USUARIO                    = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresVendas->DCAV_ESTACAO                    = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresVendas->DCAV_ESTACAO_CONTA              = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresVendas->DCAV_ASSINATURA                 = $this->gerarAssinaturaRegistro($eDocumentosAuxiliaresVendas->DCAV_ATUALIZACAO, $eDocumentosAuxiliaresVendas->DCAV_USUARIO, $eDocumentosAuxiliaresVendas->DCAV_ESTACAO, $eDocumentosAuxiliaresVendas->DCAV_ESTACAO_CONTA);

                        $eDocumentosAuxiliaresVendas->save();

                        $pedidoConsistente = true;

                        $dcavId = EDocumentosAuxiliaresVendas::select('DCAV_ID')->where('DCAV_E_COM_PLUS_ID', $pedido['_id'])
                            ->get()
                            ->first();

                        $dcavId = $dcavId['DCAV_ID'];

                    } catch (\Throwable $th) {
                        $this->gerarLog('Capa do pedido ' . $pedido['_id'] . ' não foi importada. Detalhes: ' . $th->getMessage());
                        continue;
                    }

                    /* Pagamentos */
                    try {
                        $formaPagamento = mb_strtolower(converterTextoParaUTF8($pedido['forma_pagamento']));

                        switch ($formaPagamento) {
                            case 'boleto bancário':
                                $formaPagamentoId = 6;
                                break;

                            case 'paypal':
                                $formaPagamentoId = 31;
                                break;

                            default:
                                $formaPagamentoId = 32;
                                break;
                        }

                        $eDocumentosAuxiliaresPagar = new EDocumentosAuxiliaresPagar();

                        $eDocumentosAuxiliaresPagar->DCAV_ID                         = $dcavId;
                        $eDocumentosAuxiliaresPagar->CPRC_ID                         = $configSistema['CPRC_ID'];
                        $eDocumentosAuxiliaresPagar->EMPR_ID                         = $configSistema['EMPR_ID'];
                        $eDocumentosAuxiliaresPagar->UNEM_ID                         = $configSistema['UNEM_ID'];
                        $eDocumentosAuxiliaresPagar->USRS_ID                         = env('USRS_ID', 0);
                        $eDocumentosAuxiliaresPagar->FRPG_ID                         = $formaPagamentoId;
                        $eDocumentosAuxiliaresPagar->PESS_ID                         = env('PESS_ID', 0);
                        $eDocumentosAuxiliaresPagar->PESS_PAGD_ID                    = env('PESS_ID', 0);
                        $eDocumentosAuxiliaresPagar->DCAP_CONFIRMADO_LIBERADO        = $statusFinanceiro == 'Pago' ? 'S' : 'N';
                        $eDocumentosAuxiliaresPagar->DCAP_STATUS_FINANCEIRO_E_COMMER = $statusFinanceiro;
                        $eDocumentosAuxiliaresPagar->DCAP_FORMA_PAGAMENTO            = ucwords(converterTextoParaWindows1252($formaPagamento));
                        $eDocumentosAuxiliaresPagar->DCAP_DATA_REFERENCIA            = $dataReferencia;
                        $eDocumentosAuxiliaresPagar->DCAP_DATA_VENCIMENTO            = $dataVencimento;
                        $eDocumentosAuxiliaresPagar->DCAP_VALOR                      = floatval($pedido['montante']['total']);
                        $eDocumentosAuxiliaresPagar->DCAP_VALOR_TOTAL                = floatval($pedido['montante']['total']);
                        $eDocumentosAuxiliaresPagar->DCAV_E_COM_PLUS_ID              = $pedido['_id'];
                        $eDocumentosAuxiliaresPagar->DCAP_ATUALIZACAO                = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje);
                        $eDocumentosAuxiliaresPagar->DCAP_USUARIO                    = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresPagar->DCAP_ESTACAO                    = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresPagar->DCAP_ESTACAO_CONTA              = env('NOME_ESTACAO_USUARIO', '');
                        $eDocumentosAuxiliaresPagar->DCAP_ASSINATURA                 = $this->gerarAssinaturaRegistro($eDocumentosAuxiliaresPagar->DCAP_ATUALIZACAO, $eDocumentosAuxiliaresPagar->DCAP_USUARIO, $eDocumentosAuxiliaresPagar->DCAP_ESTACAO, $eDocumentosAuxiliaresPagar->DCAP_ESTACAO_CONTA);

                        $eDocumentosAuxiliaresPagar->save();
                    } catch (\Throwable $th) {
                        $this->gerarLog('Pagamento do pedido ' . $pedido['_id'] . ' não foi importado. Detalhes: ' . $th->getMessage());
                        $pedidoConsistente = false;
                    }

                    /* Itens do pedido */
                    try {
                        $numeroItem = 1;

                        foreach ($pedido['itens_pedido'] as $item) {
                            $dadosProduto = DB::table('E_PRODUTOS_UNIDADES AS PRUN')
                                ->join('E_PRODUTOS AS PROD', 'PRUN.PROD_ID', '=', 'PROD.PROD_ID')
                                ->join('E_PRODUTOS_MARCAS AS PRMC', 'PRUN.PRMC_ID', '=', 'PRMC.PRMC_ID')
                                ->join('E_PRODUTOS_GRADES AS PRGD', 'PRUN.PRGD_ID', '=', 'PRGD.PRGD_ID')
                                ->join('E_UNIDADES_MEDIDAS AS UNMD', 'PRUN.UNMD_ID', '=', 'UNMD.UNMD_ID')
                                ->join('E_CONFIGURACOES_SISTEMAS AS CFST', 'PRUN.UNEM_ID', '=', 'CFST.UNEM_ID')
                                ->select('PRUN.PRUN_ID', 'PRUN.PROD_ID', 'PRUN.PRMC_ID', 'PRUN.PRGD_ID', 'PRUN.PRUN_CODIGO', 'PROD.PROD_NOME_COMERCIAL', 'UNMD.UNMD_SIGLA')
                                ->where('PRUN_CODIGO', $item['sku'])
                                ->where('CFST_E_COM_PLUS_STORE_ID', env('E_COM_PLUS_STORE_ID', 0))
                                ->get()
                                ->first();

                            $ncms = DB::connection('firebird')->select('
                                SELECT
                                  NCMS.NCMS_ID,
                                  NCMS.NCMS_CODIGO
                                FROM E_NCMS NCMS
                                  INNER JOIN E_PRODUTOS PROD ON
                                    NCMS.NCMS_ID = PROD.NCMS_ID
                                  INNER JOIN E_PRODUTOS_UNIDADES PRUN ON
                                    PROD.PROD_ID = PRUN.PROD_ID
                                  INNER JOIN E_PRODUTOS_MARCAS PRMC ON
                                    PRUN.PRMC_ID = PRMC.PRMC_ID
                                  INNER JOIN E_PRODUTOS_GRADES PRGD ON
                                    PRUN.PRGD_ID = PRGD.PRGD_ID
                                WHERE
                                  PRUN.PRUN_ID = ?
                                AND
                                  PRUN.UNEM_ID = ?', [
                                $item['sku'],
                                $configSistema['UNEM_ID'],
                            ]);

                            $dataHoraHoje = Carbon::now();
                            $dataHoraHoje = $dataHoraHoje->toDateTimeString();

                            $eDocumentosAuxiliaresItens = new EDocumentosAuxiliaresItens();

                            $eDocumentosAuxiliaresItens->DCAV_ID                     = $dcavId;
                            $eDocumentosAuxiliaresItens->CPRC_ID                     = $configSistema['CPRC_ID'];
                            $eDocumentosAuxiliaresItens->EMPR_ID                     = $configSistema['EMPR_ID'];
                            $eDocumentosAuxiliaresItens->UNEM_ID                     = $configSistema['UNEM_ID'];
                            $eDocumentosAuxiliaresItens->USRS_VEND_ID                = intval(env('USRS_ID', 0));
                            $eDocumentosAuxiliaresItens->PROD_ID                     = intval($item['sku']);
                            $eDocumentosAuxiliaresItens->PRMC_ID                     = $dadosProduto->PRMC_ID;
                            $eDocumentosAuxiliaresItens->PRGD_ID                     = $dadosProduto->PRGD_ID;
                            $eDocumentosAuxiliaresItens->PRUN_ID                     = $dadosProduto->PRUN_ID;
                            $eDocumentosAuxiliaresItens->NCMS_ID                     = is_null($ncms) ? 0 : $ncms[0]->NCMS_ID;
                            $eDocumentosAuxiliaresItens->DCAI_ITEM                   = $numeroItem;
                            $eDocumentosAuxiliaresItens->DCAI_CODIGO_PRODUTO_SERVICO = $dadosProduto->PRUN_CODIGO;
                            $eDocumentosAuxiliaresItens->DCAI_NOME_PRODUTO_SERVICO   = converterTextoParaWindows1252($dadosProduto->PROD_NOME_COMERCIAL);
                            $eDocumentosAuxiliaresItens->DCAI_UNIDADE                = strtoupper($dadosProduto->UNMD_SIGLA);
                            $eDocumentosAuxiliaresItens->DCAI_QUANTIDADE             = floatval($item['quantity']);
                            $eDocumentosAuxiliaresItens->DCAI_VALOR_DESCONTO_VENDA   = isset($pedido['montante']['discount']) ? floatval($pedido['montante']['discount']) : 0;
                            $eDocumentosAuxiliaresItens->DCAI_VALOR_TOTAL_BRUTO      = isset($pedido['montante']['total']) ? floatval($pedido['montante']['total']) : 0;
                            $eDocumentosAuxiliaresItens->DCAI_VALOR_TOTAL_LIQUIDO    = isset($pedido['montante']['subtotal']) ? floatval($pedido['montante']['subtotal']) : 0;
                            $eDocumentosAuxiliaresItens->DCAI_VALOR_FRETE_VENDA      = isset($pedido['montante']['freight']) ? floatval($pedido['montante']['freight']) : 0;
                            $eDocumentosAuxiliaresItens->DCAI_VALOR_UNITARIO_VENDA   = isset($pedido['montante']['subtotal']) ? floatval($pedido['montante']['subtotal']) : 0;
                            $eDocumentosAuxiliaresItens->DCAI_PRAZO_MEDIO_ENTREGA    = $dataPrazoMedioEntrega;
                            $eDocumentosAuxiliaresItens->DCAI_DATA_EMISSAO           = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje);
                            $eDocumentosAuxiliaresItens->DCAI_NCM                    = is_null($ncms) ? '99999999999' : $ncms[0]->NCMS_CODIGO;
                            $eDocumentosAuxiliaresItens->DCAV_E_COM_PLUS_ID          = $pedido['_id'];
                            $eDocumentosAuxiliaresItens->DCAI_E_COM_PLUS_ID          = $item['_id'];
                            $eDocumentosAuxiliaresItens->DCAI_ATUALIZACAO            = Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje);
                            $eDocumentosAuxiliaresItens->DCAI_USUARIO                = env('NOME_ESTACAO_USUARIO', '');
                            $eDocumentosAuxiliaresItens->DCAI_ESTACAO                = env('NOME_ESTACAO_USUARIO', '');
                            $eDocumentosAuxiliaresItens->DCAI_ESTACAO_CONTA          = env('NOME_ESTACAO_USUARIO', '');
                            $eDocumentosAuxiliaresItens->DCAI_ASSINATURA             = $this->gerarAssinaturaRegistro($eDocumentosAuxiliaresItens->DCAI_ATUALIZACAO, $eDocumentosAuxiliaresItens->DCAI_USUARIO, $eDocumentosAuxiliaresItens->DCAI_ESTACAO, $eDocumentosAuxiliaresItens->DCAI_ESTACAO_CONTA);

                            $eDocumentosAuxiliaresItens->save();

                            $numeroItem++;
                        }
                    } catch (\Throwable $th) {
                        $this->gerarLog('Item/itens do pedido ' . $pedido['_id'] . ' não foi/foram importados. Detalhes: ' . $th->getMessage());
                        $pedidoConsistente = false;
                    }

                    if ($pedidoConsistente) {
                        $this->gerarLog('Pedido ' . $pedido['_id'] . ' importado.');
                    } else {
                        $this->gerarLog('Pedido ' . $pedido['_id'] . ' foi importado com inconsistência.');
                    }

                    $pedidosImportados++;
                } else {
                    /* Atualizar status financeiro do pedido */

                    try {
                        /* Status financeiro */
                        $statusFinanceiro = '';

                        switch ($pedido['status_financeiro']) {
                            case 'pending':
                                $statusFinanceiro = 'Pendente';
                                break;

                            case 'under_analysis':
                                $statusFinanceiro = 'Sob análise';
                                break;

                            case 'authorized':
                                $statusFinanceiro = 'Autorizado';
                                break;

                            case 'unauthorized':
                                $statusFinanceiro = 'Não autorizado';
                                break;

                            case 'partially_paid':
                                $statusFinanceiro = 'Parcialmente pago';
                                break;

                            case 'paid':
                                $statusFinanceiro = 'Pago';
                                break;

                            case 'in_dispute':
                                $statusFinanceiro = 'Em disputa';
                                break;

                            case 'partially_refunded':
                                $statusFinanceiro = 'Parcialmente ressarcido';
                                break;

                            case 'refunded':
                                $statusFinanceiro = 'Ressarcido';
                                break;

                            case 'voided':
                                $statusFinanceiro = 'Cancelado';
                                break;

                            case 'unknown':
                                $statusFinanceiro = 'Desconhecido';
                                break;
                        }

                        /* Pagamento */
                        $eDocumentosAuxiliaresPagar = EDocumentosAuxiliaresPagar::select('DCAP_STATUS_FINANCEIRO_E_COMMER')->where('DCAV_E_COM_PLUS_ID', $pedido['_id'])->get()->first();

                        if (isset($eDocumentosAuxiliaresVendas) && isset($eDocumentosAuxiliaresPagar)) {
                            if ($eDocumentosAuxiliaresVendas->DCAV_STATUS_FINANCEIRO_E_COMMER != $statusFinanceiro && $eDocumentosAuxiliaresPagar->DCAP_STATUS_FINANCEIRO_E_COMMER != $statusFinanceiro) {
                                $dataHoraHoje = Carbon::now();
                                $dataHoraHoje = $dataHoraHoje->toDateTimeString();

                                /* Capa */
                                EDocumentosAuxiliaresVendas::where('DCAV_E_COM_PLUS_ID', $pedido['_id'])
                                    ->update(
                                        [
                                            'DCAV_STATUS_FINANCEIRO_E_COMMER' => $statusFinanceiro,
                                            'DCAV_OBSERVACAO'                 => converterTextoParaWindows1252($pedido['notas_pedido'] . ' Status financeiro: ' . $statusFinanceiro),
                                            'DCAV_ATUALIZACAO'                => Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje),
                                            'DCAV_USUARIO'                    => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAV_ESTACAO'                    => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAV_ESTACAO_CONTA'              => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAV_ASSINATURA'                 => $this->gerarAssinaturaRegistro($eDocumentosAuxiliaresVendas->DCAV_ATUALIZACAO, $eDocumentosAuxiliaresVendas->DCAV_USUARIO, $eDocumentosAuxiliaresVendas->DCAV_ESTACAO, $eDocumentosAuxiliaresVendas->DCAV_ESTACAO_CONTA),
                                        ]);

                                /* Pagamento */
                                EDocumentosAuxiliaresPagar::where('DCAV_E_COM_PLUS_ID', $pedido['_id'])
                                    ->update(
                                        [
                                            'DCAP_CONFIRMADO_LIBERADO'        => $statusFinanceiro == 'Pago' ? 'S' : 'N',
                                            'DCAP_STATUS_FINANCEIRO_E_COMMER' => $statusFinanceiro,
                                            'DCAP_ATUALIZACAO'                => Carbon::createFromFormat('Y-m-d H:i:s', $dataHoraHoje),
                                            'DCAP_USUARIO'                    => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAP_ESTACAO'                    => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAP_ESTACAO_CONTA'              => env('NOME_ESTACAO_USUARIO', ''),
                                            'DCAP_ASSINATURA'                 => $this->gerarAssinaturaRegistro($eDocumentosAuxiliaresPagar->DCAP_ATUALIZACAO, $eDocumentosAuxiliaresPagar->DCAP_USUARIO, $eDocumentosAuxiliaresPagar->DCAP_ESTACAO, $eDocumentosAuxiliaresPagar->DCAP_ESTACAO_CONTA),
                                        ]);

                                $this->gerarLog('Pagamento do pedido ' . $pedido['_id'] . ' foi atualizado.');

                                $pedidosAtualizados++;
                            }
                        }
                    } catch (\Throwable $th) {
                        $this->gerarLog('Pagamento do pedido ' . $pedido['_id'] . ' não foi atualizado. Detalhes: ' . $th->getMessage());
                        $pedidoConsistente = false;
                    }
                }
            }

            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto

            $resposta = $pedidosImportados > 0 || $pedidosAtualizados > 0 ? $pedidosImportados . ' pedido(s) importado(s). ' . $pedidosAtualizados . ' pedido(s) atualizado(s).' : 'Nenhum pedido foi importado/atualizado ou o(s) status financeiro dos pedido(s) não sofreu/sofreram alteração/alterações até o momento.';
            return $resposta;

        } catch (\Throwable $th) {
            ini_set('max_execution_time', '60'); // 60 segundos == 1 minuto
            $this->gerarLog('Erro ao importar/atualizar pedidos. Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Enviar imagem apx storage
     *
     * @param mixed $configSistema
     * @param string $nomeArquivoExtensao
     * @param string $nomeImagem
     * @param string $extensao
     * @return string
     */
    public function enviarImagemApxStorage($configSistema, $pathArquivo, $nomeImagem, $extensao)
    {
        try {
            $dados    = [];
            $dados[0] = [
                'CFST_E_COM_PLUS_STORE_ID' => env('E_COM_PLUS_STORE_ID', 0),
                'CFST_E_COM_PLUS_ID'       => $configSistema['CFST_E_COM_PLUS_ID'],
                'CFST_E_COM_PLUS_API_KEY'  => $configSistema['CFST_E_COM_PLUS_API_KEY'],
            ];

            $validacao = EConfiguracoesSistemasController::validarTokenAcessoEComPlus($dados);

            $body[] = implode("\r\n", [
                "Content-Disposition: form-data; name=\"file\"; filename=\"{$nomeImagem}\"",
                "Content-Type: image/{$extensao}",
                "",
                file_get_contents($pathArquivo),
            ]);

            do {
                $boundary = "---------------------" . md5(mt_rand() . microtime());
            } while (preg_grep("/{$boundary}/", $body));

            array_walk($body, function (&$part) use ($boundary) {
                $part = "--{$boundary}\r\n{$part}";
            });

            $body[] = "--{$boundary}--";
            $body[] = "";

            $headers = [
                "X-Store-ID: " . env('E_COM_PLUS_STORE_ID', 0),
                "X-Access-Token: " . $validacao['X-Access-Token'],
                "X-My-ID: " . $configSistema['CFST_E_COM_PLUS_ID'],
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, obterBaseUriEcomPlusImagens());
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, implode("\r\n", $body));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($headers, [
                "Content-Type: multipart/form-data; boundary={$boundary}",
            ]));

            $responseEnvio = curl_exec($ch);
            curl_close($ch);

            return $responseEnvio;
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao enviar imagem. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
            return '';
        }
    }

    /**
     * Salvar imagem de produto
     *
     * @param string $responseEnvio
     * @param int $unemId
     * @param int $prodId
     * @param int $prmcId
     * @param int $prgdId
     * @param int $prunId
     * @param string $nomeImagem
     * @param string $extensao
     * @return void
     */
    public function salvarImagemProduto($responseEnvio, $unemId, $prodId, $prmcId, $prgdId, $prunId, $nomeImagem, $extensao)
    {
        $response = json_decode($responseEnvio, true);

        $enviada = false;

        // try {
        //     $eArquivosECommerce = new EArquivosECommerce();
        //     // Zoom
        //     $eArquivosECommerce->UNEM_ID               = $unemId;
        //     $eArquivosECommerce->PROD_ID               = $prodId;
        //     $eArquivosECommerce->PRMC_ID               = $prmcId;
        //     $eArquivosECommerce->PRGD_ID               = $prgdId;
        //     $eArquivosECommerce->PRUN_ID               = $prunId;
        //     $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
        //     $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['zoom']['url'];
        //     $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'zoom_' . $nomeImagem;
        //     $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        //     $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);
        //     $eArquivosECommerce->save();
        //     $enviada = true;
        // } catch (\Throwable $th) {
        //     $this->gerarLog('Erro ao salvar imagem de produto tamanho zoom. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        // }

        try {
            $eArquivosECommerce = new EArquivosECommerce();

            // Big
            $eArquivosECommerce->UNEM_ID               = $unemId;
            $eArquivosECommerce->PROD_ID               = $prodId;
            $eArquivosECommerce->PRMC_ID               = $prmcId;
            $eArquivosECommerce->PRGD_ID               = $prgdId;
            $eArquivosECommerce->PRUN_ID               = $prunId;
            $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
            $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['big']['url'];
            $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'big_' . $nomeImagem . '.webp';
            $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
            $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);

            $eArquivosECommerce->save();

            $enviada = true;
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao salvar imagem de produto tamanho big. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        }

        // try {
        //     $eArquivosECommerce = new EArquivosECommerce();
        //     // Normal
        //     $eArquivosECommerce->UNEM_ID               = $unemId;
        //     $eArquivosECommerce->PROD_ID               = $prodId;
        //     $eArquivosECommerce->PRMC_ID               = $prmcId;
        //     $eArquivosECommerce->PRGD_ID               = $prgdId;
        //     $eArquivosECommerce->PRUN_ID               = $prunId;
        //     $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
        //     $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['normal']['url'];
        //     $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'normal_' . $nomeImagem . '.webp';
        //     $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        //     $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);
        //     $eArquivosECommerce->save();
        // } catch (\Throwable $th) {
        //     $this->gerarLog('Erro ao salvar imagem de produto tamanho normal. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        // }

        try {
            if ($extensao == 'jpeg') {
                $extensao = 'jpg';
            }

            if ($enviada) {
                rename(env('PATH_IMAGENS_PRODUTOS', '') . $nomeImagem . '.' . $extensao, env('PATH_IMAGENS_ENVIADAS_PRODUTOS', '') . $nomeImagem . '.' . $extensao);
                $this->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' movida para ' . env('PATH_IMAGENS_ENVIADAS_PRODUTOS', ''));
            } else {
                rename(env('PATH_IMAGENS_PRODUTOS', '') . $nomeImagem . '.' . $extensao, env('PATH_IMAGENS_ERROS_PRODUTOS', '') . $nomeImagem . '.' . $extensao);
                $this->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' movida para ' . env('PATH_IMAGENS_ERROS_PRODUTOS', ''));
            }
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao mover imagem de produto de pasta (Enviadas e Erros). Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        }
    }

    /**
     * Salvar imagem de marca
     *
     * @param string $responseEnvio
     * @param int $unemId
     * @param int $marcId
     * @param string $nomeImagem
     * @param string $extensao
     * @param string $largura
     * @param string $altura
     * @return void
     */
    public function salvarImagemMarca($responseEnvio, $unemId, $marcId, $nomeImagem, $extensao, $largura, $altura)
    {
        $response = json_decode($responseEnvio, true);

        $enviada = false;

        // try {
        //     $eArquivosECommerce = new EArquivosECommerce();
        //     // Zoom
        //     $eArquivosECommerce->UNEM_ID               = $unemId;
        //     $eArquivosECommerce->MARC_ID               = $marcId;
        //     $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
        //     $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['zoom']['url'];
        //     $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'zoom_' . $nomeImagem;
        //     $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        //     $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);
        //     $eArquivosECommerce->save();
        //     $enviada = true;
        // } catch (\Throwable $th) {
        //     $this->gerarLog('Erro ao salvar imagem de produto tamanho zoom. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        // }

        try {
            $eArquivosECommerce = new EArquivosECommerce();

            // Big
            $eArquivosECommerce->UNEM_ID               = $unemId;
            $eArquivosECommerce->MARC_ID               = $marcId;
            $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
            $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['big']['url'];
            $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'big_' . $nomeImagem . '.webp';
            $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
            $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
            $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);

            $eArquivosECommerce->save();

            $enviada = true;
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao salvar imagem de produto tamanho big. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        }

        // try {
        //     $eArquivosECommerce = new EArquivosECommerce();
        //     // Normal
        //     $eArquivosECommerce->UNEM_ID               = $unemId;
        //     $eArquivosECommerce->MARC_ID               = $marcId;
        //     $eArquivosECommerce->AREC_E_COM_PLUS_ID    = gerarIdDinamica();
        //     $eArquivosECommerce->AREC_URL_ANEXO        = $response['picture']['normal']['url'];
        //     $eArquivosECommerce->AREC_URL_ANEXO_TITULO = 'normal_' . $nomeImagem . '.webp';
        //     $eArquivosECommerce->AREC_ATUALIZACAO      = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
        //     $eArquivosECommerce->AREC_USUARIO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO          = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ESTACAO_CONTA    = env('NOME_ESTACAO_USUARIO', '');
        //     $eArquivosECommerce->AREC_ASSINATURA       = $this->gerarAssinaturaRegistro($eArquivosECommerce->AREC_ATUALIZACAO, $eArquivosECommerce->AREC_USUARIO, $eArquivosECommerce->AREC_ESTACAO, $eArquivosECommerce->AREC_ESTACAO_CONTA);
        //     $eArquivosECommerce->save();
        // } catch (\Throwable $th) {
        //     $this->gerarLog('Erro ao salvar imagem de produto tamanho normal. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        // }

        try {
            // Atualiza dados da marca
            $eMarcas = EMarcas::find($marcId);

            $eMarcas->MARC_LOGO_URL      = $response['picture']['normal']['url'];
            $eMarcas->MARC_LOGO_TAMANHO  = $largura . 'x' . $altura;
            $eMarcas->MARC_ATUALIZACAO   = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());
            $eMarcas->MARC_USUARIO       = env('NOME_ESTACAO_USUARIO', '');
            $eMarcas->MARC_ESTACAO       = env('NOME_ESTACAO_USUARIO', '');
            $eMarcas->MARC_ESTACAO_CONTA = env('NOME_ESTACAO_USUARIO', '');
            $eMarcas->MARC_ASSINATURA    = $this->gerarAssinaturaRegistro($eMarcas->MARC_ATUALIZACAO, $eMarcas->MARC_USUARIO, $eMarcas->MARC_ESTACAO, $eMarcas->MARC_ESTACAO_CONTA);

            $eMarcas->save();
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao atualizar marca. Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        }

        try {
            if ($extensao == 'jpeg') {
                $extensao = 'jpg';
            }

            if ($enviada) {
                rename(env('PATH_IMAGENS_MARCAS', '') . $nomeImagem . '.' . $extensao, env('PATH_IMAGENS_ENVIADAS_MARCAS', '') . $nomeImagem . '.' . $extensao);
                $this->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' movida para ' . env('PATH_IMAGENS_ENVIADAS_MARCAS', ''));
            } else {
                rename(env('PATH_IMAGENS_MARCAS', '') . $nomeImagem . '.' . $extensao, env('PATH_IMAGENS_ERROS_MARCAS', '') . $nomeImagem . '.' . $extensao);
                $this->gerarLog('Imagem ' . $nomeImagem . '.' . $extensao . ' movida para ' . env('PATH_IMAGENS_ERROS_MARCAS', ''));
            }
        } catch (\Throwable $th) {
            $this->gerarLog('Erro ao mover imagem de marca de pasta (Enviadas e Erros). Imagem: ' . $nomeImagem . '.' . $extensao . ' Detalhes: ' . $th->getMessage());
        }
    }

}

<?php

/**
 * Obter Base URI da API de produção do e.com-plus
 *
 * @return string
 */
function obterBaseUriEcomPlus()
{
    return 'https://api.e-com.plus/v1/';
}

/**
 * Obter Base URI da API de produção do e.com-plus - Imagens de produtos
 *
 * @return string
 */
function obterBaseUriEcomPlusImagens()
{
    return 'https://apx-storage.e-com.plus/' . env('E_COM_PLUS_STORE_ID', 0) . '/api/v1/upload.json';
}

/**
 * Obter Base URI da API de sandbox do e.com-plus
 *
 * @return string
 */
function obterBaseUriEcomPlusSandbox()
{
    return 'https://sandbox.e-com.plus/v1/';
}

/**
 * Converte string para UTF8
 * @param  string  $texto
 * @return string
 */
function converterTextoParaUTF8($texto)
{
    $encoding = mb_detect_encoding($texto, mb_detect_order(), false);

    if ($encoding == "UTF-8") {
        $texto = mb_convert_encoding($texto, 'UTF-8', 'UTF-8');
    }

    return iconv(mb_detect_encoding($texto, mb_detect_order(), false), "UTF-8//IGNORE", $texto);
}

/**
 * Converter texto para Windows-1252
 * @param string $texto
 * @return string
 */
function converterTextoParaWindows1252($texto)
{
    if (env('DB_CHARSET', '') == 'WIN1252') {
        return mb_convert_encoding($texto, 'Windows-1252', 'UTF-8');
    } else {
        return $texto;
    }
}

/**
 * Converter texto de CP1252 para UTF-8
 * @param  string  $input
 * @param  string  $default
 * @param  array  $replace
 * @return mixed
 */
function converterTextoCp1252ToUtf8($input, $default = '', $replace = [])
{
    if ($input === null || $input == '') {
        return $default;
    }

    // https://en.wikipedia.org/wiki/UTF-8
    // https://en.wikipedia.org/wiki/ISO/IEC_8859-1
    // https://en.wikipedia.org/wiki/Windows-1252
    // http://www.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP1252.TXT
    $encoding = mb_detect_encoding($input, array('Windows-1252', 'ISO-8859-1'), true);
    if ($encoding == 'ISO-8859-1' || $encoding == 'Windows-1252') {
        /*
         * Use the search/replace arrays if a character needs to be replaced with
         * something other than its Unicode equivalent.
         */

        /*$replace = array(
        128 => "&#x20AC;",      // http://www.fileformat.info/info/unicode/char/20AC/index.htm EURO SIGN
        129 => "",              // UNDEFINED
        130 => "&#x201A;",      // http://www.fileformat.info/info/unicode/char/201A/index.htm SINGLE LOW-9 QUOTATION MARK
        131 => "&#x0192;",      // http://www.fileformat.info/info/unicode/char/0192/index.htm LATIN SMALL LETTER F WITH HOOK
        132 => "&#x201E;",      // http://www.fileformat.info/info/unicode/char/201e/index.htm DOUBLE LOW-9 QUOTATION MARK
        133 => "&#x2026;",      // http://www.fileformat.info/info/unicode/char/2026/index.htm HORIZONTAL ELLIPSIS
        134 => "&#x2020;",      // http://www.fileformat.info/info/unicode/char/2020/index.htm DAGGER
        135 => "&#x2021;",      // http://www.fileformat.info/info/unicode/char/2021/index.htm DOUBLE DAGGER
        136 => "&#x02C6;",      // http://www.fileformat.info/info/unicode/char/02c6/index.htm MODIFIER LETTER CIRCUMFLEX ACCENT
        137 => "&#x2030;",      // http://www.fileformat.info/info/unicode/char/2030/index.htm PER MILLE SIGN
        138 => "&#x0160;",      // http://www.fileformat.info/info/unicode/char/0160/index.htm LATIN CAPITAL LETTER S WITH CARON
        139 => "&#x2039;",      // http://www.fileformat.info/info/unicode/char/2039/index.htm SINGLE LEFT-POINTING ANGLE QUOTATION MARK
        140 => "&#x0152;",      // http://www.fileformat.info/info/unicode/char/0152/index.htm LATIN CAPITAL LIGATURE OE
        141 => "",              // UNDEFINED
        142 => "&#x017D;",      // http://www.fileformat.info/info/unicode/char/017d/index.htm LATIN CAPITAL LETTER Z WITH CARON
        143 => "",              // UNDEFINED
        144 => "",              // UNDEFINED
        145 => "&#x2018;",      // http://www.fileformat.info/info/unicode/char/2018/index.htm LEFT SINGLE QUOTATION MARK
        146 => "&#x2019;",      // http://www.fileformat.info/info/unicode/char/2019/index.htm RIGHT SINGLE QUOTATION MARK
        147 => "&#x201C;",      // http://www.fileformat.info/info/unicode/char/201c/index.htm LEFT DOUBLE QUOTATION MARK
        148 => "&#x201D;",      // http://www.fileformat.info/info/unicode/char/201d/index.htm RIGHT DOUBLE QUOTATION MARK
        149 => "&#x2022;",      // http://www.fileformat.info/info/unicode/char/2022/index.htm BULLET
        150 => "&#x2013;",      // http://www.fileformat.info/info/unicode/char/2013/index.htm EN DASH
        151 => "&#x2014;",      // http://www.fileformat.info/info/unicode/char/2014/index.htm EM DASH
        152 => "&#x02DC;",      // http://www.fileformat.info/info/unicode/char/02DC/index.htm SMALL TILDE
        153 => "&#x2122;",      // http://www.fileformat.info/info/unicode/char/2122/index.htm TRADE MARK SIGN
        154 => "&#x0161;",      // http://www.fileformat.info/info/unicode/char/0161/index.htm LATIN SMALL LETTER S WITH CARON
        155 => "&#x203A;",      // http://www.fileformat.info/info/unicode/char/203A/index.htm SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
        156 => "&#x0153;",      // http://www.fileformat.info/info/unicode/char/0153/index.htm LATIN SMALL LIGATURE OE
        157 => "",              // UNDEFINED
        158 => "&#x017e;",      // http://www.fileformat.info/info/unicode/char/017E/index.htm LATIN SMALL LETTER Z WITH CARON
        159 => "&#x0178;",      // http://www.fileformat.info/info/unicode/char/0178/index.htm LATIN CAPITAL LETTER Y WITH DIAERESIS
        );*/

        if (count($replace) != 0) {
            $find = array();
            foreach (array_keys($replace) as $key) {
                $find[] = chr($key);
            }
            $input = str_replace($find, array_values($replace), $input);
        }
        /*
         * Because ISO-8859-1 and CP1252 are identical except for 0x80 through 0x9F
         * and control characters, always convert from Windows-1252 to UTF-8.
         */
        $input = iconv('Windows-1252', 'UTF-8//IGNORE', $input);
        if (count($replace) != 0) {
            $input = html_entity_decode($input);
        }
    }
    return $input;
}

/**
 * Formatar slug para formato aceito pela e-com.plus
 * @param  string  $slug
 * @return string
 */
function formatarSlug($slug)
{
    $novoSlug = mb_strtolower(converterTextoParaUTF8($slug));
    $novoSlug = preg_replace('/[áàãâä]/ui', 'a', $novoSlug);
    $novoSlug = preg_replace('/[éèêë]/ui', 'e', $novoSlug);
    $novoSlug = preg_replace('/[íìîï]/ui', 'i', $novoSlug);
    $novoSlug = preg_replace('/[óòõôö]/ui', 'o', $novoSlug);
    $novoSlug = preg_replace('/[úùûü]/ui', 'u', $novoSlug);
    $novoSlug = preg_replace('/[ç]/ui', 'c', $novoSlug);
    $novoSlug = preg_replace('/[^a-z0-9]/i', '_', $novoSlug);
    $novoSlug = preg_replace('/_+/', '_', $novoSlug);

    return $novoSlug;
}

/**
 * Formatar nomes para formato aceito pela e-com.plus
 * @param  string  $texto
 * @return string
 */
function formatarTexto($texto)
{
    $novoTexto = utf8_encode($texto);
    $novoTexto = preg_replace('/[áàãâä]/ui', 'a', $novoTexto);
    $novoTexto = preg_replace('/[éèêë]/ui', 'e', $novoTexto);
    $novoTexto = preg_replace('/[íìîï]/ui', 'i', $novoTexto);
    $novoTexto = preg_replace('/[óòõôö]/ui', 'o', $novoTexto);
    $novoTexto = preg_replace('/[úùûü]/ui', 'u', $novoTexto);
    $novoTexto = preg_replace('/[ç]/ui', 'c', $novoTexto);
    $novoTexto = ucwords(mb_strtolower($novoTexto));

    return $novoTexto;
}

/**
 * Gerar ID dinâmica
 * @param  int  $length
 * @return string
 */
function gerarIdDinamica($length = 24)
{
    $characters       = '0123456789abcdef';
    $charactersLength = strlen($characters);
    $randomString     = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

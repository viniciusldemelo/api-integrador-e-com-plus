<?php

use App\Http\Controllers\EDocumentosAuxiliaresVendasController;
use App\Http\Controllers\EGruposController;
use App\Http\Controllers\ELogsController;
use App\Http\Controllers\EMarcasController;
use App\Http\Controllers\EProdutosController;
use App\Http\Controllers\TesteController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/* Logs */
Route::get('listar_logs', [ELogsController::class, 'obterLogs']);
Route::post('salvar_log', [ELogsController::class, 'salvarLog']);

/* Produtos */
Route::get('enviar_produtos', [EProdutosController::class, 'enviarProdutos']);
Route::get('enviar_imagens_produtos', [EProdutosController::class, 'enviarImagensProdutos']);

/* Categorias */
Route::get('enviar_categorias', [EGruposController::class, 'enviarCategorias']);

/* Marcas */
Route::get('enviar_marcas', [EMarcasController::class, 'enviarMarcas']);
Route::get('enviar_imagens_marcas', [EMarcasController::class, 'enviarImagensMarcas']);

/* Pedidos */
Route::get('importar_pedidos', [EDocumentosAuxiliaresVendasController::class, 'importarPedidos']);
Route::get('listar_pedidos_importados', [EDocumentosAuxiliaresVendasController::class, 'listarPedidosImportados']);

// Route::get('teste1', [TesteController::class, 'teste1']);
// Route::get('teste2', [TesteController::class, 'teste2']);
